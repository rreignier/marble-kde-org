<!DOCTYPE html>
<html lang="en">
<?php include "head.inc"; ?>
<body>
<?php include "navigation.inc"; ?>
  <div class="container">

<!--  
  <form method="post" action="#">
    <input type="hidden" name="action" value="initialize" />
    <input type="submit" value="Initialize database" />  
  </form>
-->
  
  <form method="post" action="#">
    <input type="hidden" name="action" value="fetch" />
    <input type="submit" value="Aggregate posts" />  
  </form>
  
<!--  
  <form method="post" action="#">
    <input type="hidden" name="action" value="create" />
    
  <table>  
  <tr>
    <td><label style="display:inline;" for="user">user name:</label></td>
    <td><input type="text" name="user" id="user" value=""/></td>
  </tr>
  <tr><td colspan="2">
    <label style="display:inline;" for="isadmin">is administrator?</label>
    <input type="checkbox" name="isadmin" id="isadmin" value="1"/>
  </td></tr>
  <tr>
    <td><label style="display:inline;" for="name">real name:</label></td>
    <td><input type="text" name="name" id="name" value=""/></td>
  </tr>
  <tr>
    <td><label style="display:inline;" for="role">role:</label></td>
    <td><input type="text" name="role" id="role" value=""/></td>
  </tr>
  <tr>
    <td><label style="display:inline;" for="email">email:</label></td>
    <td><input type="text" name="email" id="email" value=""/></td>
  </tr>
  <tr>
    <td><label style="display:inline;" for="country">country:</label></td>
    <td><input type="text" name="country" id="country" value=""/></td>
  </tr>
  <tr>
    <td><label style="display:inline;" for="city">city:</label></td>
    <td><input type="text" name="city" id="city" value=""/></td>
  </tr>
  <tr>
    <td><label style="display:inline;" for="description">description:</label></td>
    <td><input type="text" name="description" id="description" value=""/></td>
  </tr>
  <tr>
    <td><label style="display:inline;" for="picture_50">picture (50x50):</label></td>
    <td><input type="text" name="picture_50" id="picture_50" value=""/></td>
  </tr>
  <tr>
    <td><label style="display:inline;" for="picture_100">picture (100x100):</label></td>
    <td><input type="text" name="picture_100" id="picture_100" value=""/></td>
  </tr>
  <tr>
    <td><label style="display:inline;" for="picture_200">picture (200x200):</label></td>
    <td><input type="text" name="picture_200" id="picture_200" value=""/></td>
  </tr>
  <tr>
    <td><label style="display:inline;" for="blogurl">blog URL:</label></td>
    <td><input type="text" name="blogurl" id="blogurl" value=""/></td>
  </tr>
  <tr>
    <td><label style="display:inline;" for="rssurl">blog RSS url:</label></td>
    <td><input type="text" name="rssurl" id="rssurl" value=""/></td>
  </tr>
  <tr>
    <td><label style="display:inline;" for="twitter">twitter:</label></td>
    <td><input type="text" name="twitter" id="twitter" value=""/></td>
  </tr>
  <tr>
    <td><label style="display:inline;" for="identica">identica:</label></td>
    <td><input type="text" name="identica" id="identica" value=""/></td>
  </tr>
  <tr>
    <td><label style="display:inline;" for="opendesktop">opendesktop:</label></td>
    <td><input type="text" name="opendesktop" id="opendesktop" value=""/></td>
  </tr>
  <tr>
    <td><label style="display:inline;" for="facebook">facebook:</label></td>
    <td><input type="text" name="facebook" id="facebook" value=""/></td>
  </tr>
  <tr>
    <td><label style="display:inline;" for="diaspora">diaspora:</label></td>
    <td><input type="text" name="diaspora" id="diaspora" value=""/></td>
  </tr>
  <tr>
    <td><label style="display:inline;" for="github">github:</label></td>
    <td><input type="text" name="github" id="github" value=""/></td>
  </tr>  
  
  <tr><td>
    <input type="submit" value="Create" />
  </td></tr>
  </table> 
  
  </form>
-->
  
  <?php
    require('contribook/lib_contribook.php');

    if (isset($_POST['action']) && $_POST['action'] === 'fetch') {
      print 'Fetching posts...';
      CONTRIBOOK_BLOG::importall();
      print ' done.';
    }
    
    return;
    
    if(isset($_POST['action']) && $_POST['action'] === 'initialize') {
      $stmt = CONTRIBOOK_DB::prepare("CREATE TABLE IF NOT EXISTS activity ( id int(11) NOT NULL AUTO_INCREMENT, type varchar(255) NOT NULL, user varchar(255) NOT NULL, message varchar(255) NOT NULL, url varchar(255) NOT NULL, timestamp int(11) NOT NULL, content text NOT NULL, PRIMARY KEY (id), KEY user (user,timestamp), KEY timestamp (timestamp) ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ; DROP TABLE IF EXISTS forum");
      $stmt->execute();
      $stmt = CONTRIBOOK_DB::prepare("CREATE TABLE IF NOT EXISTS forum ( id int(11) NOT NULL AUTO_INCREMENT, title varchar(255) NOT NULL, url varchar(255) NOT NULL, timestamp int(11) NOT NULL, PRIMARY KEY (id) ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ; DROP TABLE IF EXISTS news");
      $stmt->execute();
      $stmt = CONTRIBOOK_DB::prepare("CREATE TABLE IF NOT EXISTS news ( id int(11) NOT NULL AUTO_INCREMENT, title varchar(255) NOT NULL, url varchar(255) NOT NULL, timestamp int(11) NOT NULL, PRIMARY KEY (id) ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ; DROP TABLE IF EXISTS ocs");
      $stmt->execute();
      $stmt = CONTRIBOOK_DB::prepare("CREATE TABLE IF NOT EXISTS ocs ( id int(11) NOT NULL AUTO_INCREMENT, category int(11) NOT NULL, name varchar(255) NOT NULL, type int(11) NOT NULL, user varchar(255) NOT NULL, url varchar(255) NOT NULL, preview varchar(255) NOT NULL, timestamp int(11) NOT NULL, description text NOT NULL, PRIMARY KEY (id) ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1");
      $stmt->execute();
      $stmt = CONTRIBOOK_DB::prepare("CREATE TABLE IF NOT EXISTS users ( id int(11) NOT NULL AUTO_INCREMENT, status int(11) NOT NULL, userid varchar(255) NOT NULL, email varchar(255) NOT NULL, name varchar(255) NOT NULL, role text NOT NULL, country varchar(255) NOT NULL, city varchar(255) NOT NULL, description text NOT NULL, picture_50 varchar(255) NOT NULL, picture_100 varchar(255) NOT NULL, picture_200 varchar(255) NOT NULL, blogurl varchar(255) NOT NULL, rssurl varchar(255) NOT NULL, twitter varchar(255) NOT NULL, identica varchar(255) NOT NULL, opendesktop varchar(255) NOT NULL, facebook varchar(255) NOT NULL, diaspora varchar(255) NOT NULL, github varchar(255) NOT NULL, PRIMARY KEY (id) ) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1");
      $stmt->execute();
      $stmt = CONTRIBOOK_DB::prepare("DELETE FROM users");
      $stmt->execute();
      $stmt = CONTRIBOOK_DB::prepare("DELETE FROM activity");
      $stmt->execute();
      $stmt = CONTRIBOOK_DB::prepare("DELETE FROM forum");
      $stmt->execute();
    } elseif (isset($_POST['action']) && $_POST['action'] === 'create') {
      $stmt = CONTRIBOOK_DB::prepare("INSERT INTO users (status, email, userid, name, role, country, city, description, picture_50, picture_100, picture_200, blogurl, rssurl, twitter, identica, opendesktop, facebook, diaspora, github) VALUES (:status, :email, :user, :name, :role, :country, :city, :description, :picture_50, :picture_100, :picture_200, :blogurl, :rssurl, :twitter, :identica, :opendesktop, :facebook, :diaspora, :github)");
      $status=isset($_POST['isadmin']) ? 2 : 1;
      $stmt->bindParam(':status', $status, PDO::PARAM_INT);
      $stmt->bindParam(':email', $_POST['email'], PDO::PARAM_STR);
      $stmt->bindParam(':user', $_POST['user'], PDO::PARAM_STR);
      $stmt->bindParam(':name', $_POST['name'], PDO::PARAM_STR);
      $stmt->bindParam(':role', $_POST['role'], PDO::PARAM_STR);
      $stmt->bindParam(':country', $_POST['country'], PDO::PARAM_STR);
      $stmt->bindParam(':city', $_POST['city'], PDO::PARAM_STR);
      $stmt->bindParam(':description', $_POST['description'], PDO::PARAM_STR);
      $stmt->bindParam(':picture_50', $_POST['picture_50'], PDO::PARAM_STR);
      $stmt->bindParam(':picture_100', $_POST['picture_100'], PDO::PARAM_STR);
      $stmt->bindParam(':picture_200', $_POST['picture_200'], PDO::PARAM_STR);
      $stmt->bindParam(':blogurl', $_POST['blogurl'], PDO::PARAM_STR);
      $stmt->bindParam(':rssurl', $_POST['rssurl'], PDO::PARAM_STR);
      $stmt->bindParam(':twitter', $_POST['twitter'], PDO::PARAM_STR);
      $stmt->bindParam(':identica', $_POST['identica'], PDO::PARAM_STR);
      $stmt->bindParam(':opendesktop', $_POST['opendesktop'], PDO::PARAM_STR);
      $stmt->bindParam(':facebook', $_POST['facebook'], PDO::PARAM_STR);
      $stmt->bindParam(':diaspora', $_POST['diaspora'], PDO::PARAM_STR);
      $stmt->bindParam(':github', $_POST['github'], PDO::PARAM_STR);
      $stmt->execute();
    }
    
    $stmt=CONTRIBOOK_DB::prepare('select status, userid, email, name, blogurl, rssurl from users');
    $stmt->execute();
    
    print '<table class="table table-striped table-bordered">';
    print '<tr>';
    print '<td>userid</td><td>status</td><td>name</td><td>email</td><td>blog</td><td>rss</td>';
    print '</tr>';
    $users=array();
    foreach ($stmt as $user) {
      print '<tr>';
      print '<td>' . $user['userid'] . '</td>';
      print '<td>' . $user['status'] . '</td>';
      print '<td>' . $user['name'] . '</td>';
      print '<td>' . $user['email'] . '</td>';
      print '<td><a href="' . $user['blogurl'] . '">' . $user['blogurl'] . '</a></td>';
      print '<td><a href="' . $user['rssurl'] . '">' . $user['rssurl'] . '</a></td>';
      print '</tr>';
    }
    print '</table>';
  ?>

  </div>
  <!-- /container -->
  
  <?php include "footer.inc"; ?>

  </body>
</html>
