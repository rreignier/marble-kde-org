<!DOCTYPE html>
<html lang="en">
<?php include "head.inc"; ?>
<body>
<?php include "navigation.inc"; ?>
  <div class="container">

  <ul class="nav nav-tabs">
  <li><a href="#contributors" data-toggle="tab"><i class="icon-user"></i> Current Contributors</a></li>
  <li><a href="#contributions" data-toggle="tab"><i class="icon-time"></i> Just Done</a></li>
  <li><a href="#review-requests" data-toggle="tab"><i class="icon-eye-open"></i> Under Review</a></li>
  <li class="active"><a href="#featured-tasks" data-toggle="tab"><i class="icon-fire"></i> GSoC Tasks</a></li>
  <li><a href="#all-tasks" data-toggle="tab"><i class="icon-tasks"></i> All Tasks</a></li>
  <li><a href="#feature-requests" data-toggle="tab"><i class="icon-comment"></i> Feature Requests</a></li>
  <li><a href="#bugs" data-toggle="tab"><i class="icon-warning-sign"></i> Bugs</a></li>
  </ul>

  <div id="codeTabContent" class="tab-content">

  <div class="tab-pane fade" id="contributors" style="max-height:500px;">
  <p>
    <span id="contributors-status">Loading data, please wait...</span> Please note that this is not an exhaustive summary of Marble development, which indeed <a href="http://www.ohloh.net/p/marble/contributors/summary">Ohloh</a> provides.
  </p>
  <table class="table table-striped table-condensed table-hover table-bordered" id="table-contributors">
    <tr>
     <th>Contributor</th>
     <th>Closed Bugs</th>
    </tr>
  </table>
  </div>
  
  <div class="tab-pane fade" id="contributions" style="max-height:500px;">
  <p>
    Bugs closed in the last three months.
  </p>
  <table class="table table-striped table-condensed table-hover table-bordered" id="table-contributions">
    <tr>
     <th>Description</th>
     <th>Last Changed</th>
     <th>Details</th>
    </tr>
  </table>
  </div>
  
  <div class="tab-pane fade" id="review-requests" style="max-height:500px;">
  <p>
    Review requests contain proposed changes (patches) for review by Marble developers. <span id="review-status"></span>
  </p>
  <table class="table table-striped table-condensed table-hover table-bordered" id="table-reviews">
    <tr>
     <th>Description</th>
     <th>Last Changed</th>
     <th>Details</th>
    </tr>
  </table>
  </div>

  <div class="tab-pane fade in active" id="featured-tasks">
  <p>
  <!--
  Don't know that certain thing to work on yet? Here are some tasks waiting for someone to tackle.<br />
  You can also enter student programs we participate in as mentoring organization: <a href="http://code.google.com/intl/de/soc/">GSoC</a>, <a href="http://code.google.com/intl/de/opensource/gci/2012/index.html">Code-in</a> and <a href="http://sophia.estec.esa.int/socis2012/">SoCiS</a>. Finally, please poke us using one of the <a href="contributors-welcome.php">communication channels</a>, we don't bite <i class="icon-heart"></i>
  -->
  <p>
  Marble provides student projects in 
  <!--<a href="http://code.google.com/intl/de/opensource/gci/2012/index.html">Google Code-in</a> 2013. -->
  <a href="https://developers.google.com/open-source/soc/">Google Summer of Code</a>.
  Here's a list of current Marble tasks. Please poke us using one of the <a href="contributors-welcome.php">communication channels</a> for more details, we don't bite <i class="icon-heart"></i>
  
  </p>

  <table class="table table-striped table-condensed table-hover table-bordered" id="table-featured">
    <tr>
     <th>Description</th>
     <th>Status</th>
     <th>Last Changed</th>
     <th>Details</th>
    </tr>
  </table>
  </div>

  <div class="tab-pane fade" id="all-tasks" style="max-height:500px;">
  <p>
    <button type="button" class="btn btn-default btn-sm pull-right" style="margin-top:2px; margin-bottom:2px;">
      <a href="https://bugs.kde.org/enter_bug.cgi?product=marble&component=general&bug_severity=task">
      <span class="glyphicon glyphicon-plus-sign"></span> Add Task</a>
    </button>
    Tasks are filed by Marble developers and contain small (less than a week) units of work. Everyone is free to pick open tasks! <span id="tasks-status"></span>
  </p>
  <table class="table table-striped table-condensed table-hover table-bordered" id="table-tasks">
    <tr>
     <th>Description</th>
     <th>Status</th>
     <th>Last Changed</th>
     <th>Details</th>
    </tr>
  </table>
  </div>

  <div class="tab-pane fade" id="feature-requests" style="max-height:500px;">
  <p>
    <a class="btn btn-sm pull-right" style="margin-right:10px;margin-left:10px;" href="https://bugs.kde.org/enter_bug.cgi?product=marble&component=general&bug_severity=wishlist"><i class="icon-plus-sign"></i> Add Request</a>
    Feature requests can be filed by anyone. They contain any form of wishes related to the Marble project. <span id="wishlist-status"></span>
  </p>
  <table class="table table-striped table-condensed table-hover table-bordered" id="table-requests">
    <tr>
     <th>Description</th>
     <th>Status</th>
     <th>Last Changed</th>
     <th>Details</th>
    </tr>
  </table>
  </div>
  
  <div class="tab-pane fade" id="bugs" style="max-height:500px;">
  <p>
    <a class="btn btn-sm pull-right" style="margin-right:10px;margin-left:10px;" href="https://bugs.kde.org/enter_bug.cgi?product=marble&component=general&bug_severity=normal"><i class="icon-plus-sign"></i> File Bug</a>
    Feature requests can be filed by anyone. They contain any form of wishes related to the Marble project. <span id="wishlist-status"></span>
  </p>
  <table class="table table-striped table-condensed table-hover table-bordered" id="table-bugs">
    <tr>
     <th>Description</th>
     <th>Status</th>
     <th>Last Changed</th>
     <th>Details</th>
    </tr>
  </table>
  </div>
  
  </div>
  </div>
  <!-- /container -->
  
  <?php include "footer.inc"; ?>

 <script>
  var $ = jQuery.noConflict();
  $(document).ready(function() {
    if (location.hash !== '') $('a[href="' + location.hash + '"]').tab('show');
    return $('a[data-toggle="tab"]').on('shown', function(e) {
      return location.hash = $(e.target).attr('href').substr(1);
    });
  });
  </script>

  <script type="text/javascript" src="js/bugzilla.js"></script>
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.0/jquery-ui.min.js"></script>
  <script>
    $.getJSON('https://bugs.kde.org/jsonrpc.cgi?method=Bug.search&callback=?&params=[{"product":"marble","status":["NEW","CONFIRMED","UNCONFIRMED","ASSIGNED"]}]',
    {},
    function(bug) {
      var tableRequests = document.getElementById('table-requests');
      var tableBugs = document.getElementById('table-bugs');
      var tableTasks = document.getElementById('table-tasks');
      document.getElementById('wishlist-status').innerHTML = "Currently there are " + bug.result.bugs.length + " open feature requests. Color indicates age or status.";
      for (var i=0;i<bug.result.bugs.length;i++)
      {
        var row = null;
        var severity = bug.result.bugs[i].severity;
        if (severity == "wishlist") {
          row = tableRequests.insertRow(tableRequests.rows.length);
        } else if (severity == "task") {
          row = tableTasks.insertRow(tableTasks.rows.length);        
        } else {
          row = tableBugs.insertRow(tableBugs.rows.length);
        }
        var status = "Open";
        var statusIcon = "icon-hand-right";
        var avatar = "";
        if (bug.result.bugs[i].status === 'ASSIGNED') {
          row.setAttribute('class', 'success');
          status = "In Work";
          statusIcon = "icon-wrench";
          avatar='<img src="' + get_gravatar(bug.result.bugs[i].assigned_to, 20) + '" width="20" height="20" alt="Assigned to ' + bug.result.bugs[i].assigned_to + '" class="pull-right" />';
        } else {
          row.setAttribute('class', 'warning');
        }

        const one_day=1000*60*60*24;
        var lastChange = new Date(bug.result.bugs[i].last_change_time);
        var age = new Date().getTime() - lastChange.getTime();
        var ageDays = age / one_day;
        if (ageDays > 365) {
          row.setAttribute('class', 'error');
        }

        row.insertCell(0).innerHTML = bug.result.bugs[i].summary;
        var icon = document.createElement("i");
        icon.setAttribute('class', statusIcon);
        icon.appendChild(document.createElement('span'));
        row.insertCell(1).innerHTML = icon.outerHTML + " " + status + avatar;

        // Last changed
        row.insertCell(2).innerHTML = fuzzyDate(lastChange);

        // Bugzilla link
        var button = document.createElement("button");
        button.setAttribute("type", 'button');
        button.setAttribute("class", 'btn btn-default btn-sm');

        var link = document.createElement("a");
        link.href = 'https://bugs.kde.org/' + bug.result.bugs[i].id;
        link.innerHTML = '#' + bug.result.bugs[i].id;

        button.appendChild(link);
        row.insertCell(3).appendChild(button);
      }
    });
    
    $.getJSON('https://bugs.kde.org/jsonrpc.cgi?method=Bug.search&callback=?&params=[{"product":"marble","status":["NEW","CONFIRMED","UNCONFIRMED","ASSIGNED"]}]',
    {},
    function(bug) {
      var tableFeatured = document.getElementById('table-featured');
      for (var i=0;i<bug.result.bugs.length;i++)
      {
        var row = tableFeatured.insertRow(tableFeatured.rows.length);
        var status = "Open";
        var statusIcon = "icon-hand-right";
        var avatar = "";
        if ($.inArray("google-code-in", bug.result.bugs[i].keywords)<0)
        {
          continue;
        }
        if (bug.result.bugs[i].status === 'ASSIGNED') {
          row.setAttribute('class', 'success');
          status = "Claimed";
          statusIcon = "icon-wrench";
          avatar='<img src="' + get_gravatar(bug.result.bugs[i].assigned_to, 20) + '" width="20" height="20" alt="Assigned to ' + bug.result.bugs[i].assigned_to + '" />';
        } else {
          row.setAttribute('class', 'warning');
        }

        const one_day=1000*60*60*24;
        var lastChange = new Date(bug.result.bugs[i].last_change_time);
        var age = new Date().getTime() - lastChange.getTime();
        var ageDays = age / one_day;
        if (ageDays > 365) {
          row.setAttribute('class', 'error');
        }

        row.insertCell(0).innerHTML = bug.result.bugs[i].summary;
        var icon = document.createElement("i");
        icon.setAttribute('class', statusIcon);
        icon.appendChild(document.createElement('span'));
        row.insertCell(1).innerHTML = "<span class=\"glyphicon glyphicon-globe\">" + icon.outerHTML + " " + status + " " + avatar + "</span>";
        row.cells[1].setAttribute("style", "white-space:nowrap;");

        // Last changed
        row.insertCell(2).innerHTML = fuzzyDate(lastChange);

        // Bugzilla link
        var button = document.createElement("button");
        button.setAttribute("type", 'button');
        button.setAttribute("class", 'btn btn-default btn-sm');

        var link = document.createElement("a");
        link.href = 'https://bugs.kde.org/' + bug.result.bugs[i].id;
        link.innerHTML = 'KDE #' + bug.result.bugs[i].id;

        button.appendChild(link);
        var cell = row.insertCell(3)
        cell.setAttribute("style", "white-space:nowrap;");
        cell.appendChild(button);
        
        // GCI link
        if (bug.result.bugs[i].url.indexOf("google-melange") !== -1 && bug.result.bugs[i].url.indexOf("view") !== -1) {
            var gciButton = document.createElement("button");
            gciButton.setAttribute("type", 'button');
            gciButton.setAttribute("class", 'btn btn-default btn-sm');

            var link = document.createElement("a");
            link.href = bug.result.bugs[i].url;
            link.innerHTML = "GCI";

            gciButton.appendChild(link);
            cell.appendChild(gciButton);
        }
      }
    });

    
    function sortByDate(a, b){
      const dateA = new Date(a.last_change_time);
      const dateB = new Date(b.last_change_time);
      return dateB - dateA;
    }
    
    function sortByAssignee(a, b){
      if (a.assigned_to===b.assigned_to) return 0;
      if (a.assigned_to<b.assigned_to) return -1;
      return 1;
    }
    
    function sortByBugcount(a, b){
      return b.bugs.length - a.bugs.length;
    }

    
    var today = new Date();
    var ago = new Date();
    var contributors = new Array();
    ago.setDate(today.getDate()-90);
    $.getJSON('https://bugs.kde.org/jsonrpc.cgi?method=Bug.search&callback=?&params=[{"product":"marble","last_change_time":"' + ago.toISOString() + '","status":["RESOLVED"]}]',
    {},
    function(bug) {
      bug.result.bugs.sort(sortByDate)
      for (var i=0;i<bug.result.bugs.length;i++) {
        var table = document.getElementById('table-contributions');
        var row = table.insertRow(table.rows.length);
    
        row.insertCell(0).innerHTML = bug.result.bugs[i].summary;

        // Last changed
        var lastChange = new Date(bug.result.bugs[i].last_change_time);
        row.insertCell(1).innerHTML = fuzzyDate(lastChange);

        // Bugzilla link
        var button = document.createElement("button");
        button.setAttribute("type", 'button');
        button.setAttribute("class", 'btn btn-default btn-sm');

        var link = document.createElement("a");
        link.href = 'https://bugs.kde.org/' + bug.result.bugs[i].id;
        link.innerHTML = '#' + bug.result.bugs[i].id;

        button.appendChild(link);
        row.insertCell(2).appendChild(button);
      }
    
      bug.result.bugs.sort(sortByAssignee)
      var current = new Object();
      current.bugs = new Array();
      for (var i=0;i<bug.result.bugs.length;i++) {
        if (i>0 && current.bugs.length > 0) {
          if (current.login !== bug.result.bugs[i].assigned_to) {
            contributors.push(current);
            current = new Object();
            current.bugs = new Array();
          }
        }
        
        if (bug.result.bugs[i].dupe_of === null) {
          current.login = bug.result.bugs[i].assigned_to;
          var newBug = new Object();
          newBug.id = bug.result.bugs[i].id;
          newBug.severity = bug.result.bugs[i].severity;
          newBug.summary = bug.result.bugs[i].summary;
          current.bugs.push(newBug);
          
          if (current.bugs.length > 0 && i+1 === bug.result.bugs.length) {
            contributors.push(current);
          }
        }
      }
      
      contributors.sort(sortByBugcount)
      
      var table = document.getElementById('table-contributors');
      document.getElementById('contributors-status').innerHTML = "In the last three months, a total of " + contributors.length + " contributors closed " + bug.result.bugs.length + " bugs. Click on a contributor to view details.";
      for (var i=0;i<contributors.length;i++) {
        var row = table.insertRow(table.rows.length);

        var lid=contributors[i].login.replace(/[^a-zA-Z0-9]/g,'');
        var collapse = '<a href="#" data-toggle="collapse" data-target="#' + lid + '"><img src="' + get_gravatar(contributors[i].login, 20) + '" width="20" height="20" alt="Assigned to ' + contributors[i].login + '" /> <span id="realname-' + lid + '"> ' + contributors[i].login + '</span></a>';
        collapse += '<div id="' + lid + '" class="collapse">';
        collapse += '<table class="table table-hover table-condensed" style="margin-top:10px;">';
        for (var j=0;j<contributors[i].bugs.length;j++) {
          collapse += '<tr><td>' + contributors[i].bugs[j].severity + '</td>';
          collapse += '<td>' + contributors[i].bugs[j].summary + '</td>';
          collapse += '<td><a href="https://bugs.kde.org/' + contributors[i].bugs[j].id + '">';
          collapse += '<button type="button" class="btn btn-default btn-sm">' + contributors[i].bugs[j].id + '</button></a></td></tr>';
        }
        collapse += '</table>';
        collapse += '</div>';
        row.insertCell(0).innerHTML = collapse;
        row.insertCell(1).innerHTML = contributors[i].bugs.length;
      }
      
      var logins=new Array();
      for (var i=0; i<contributors.length; ++i) {
        logins.push('"' + contributors[i].login + '"');
      }
      
      $.getJSON('https://bugs.kde.org/jsonrpc.cgi?method=User.get&callback=?&params=[{"names":[' + logins.toString() + ']}]',
      {},
      function(user) {
        for (var i=0;i<user.result.users.length;i++) {
          if (user.result.users[i].real_name !== "") {
            var lid = user.result.users[i].name.replace(/[^a-zA-Z0-9]/g,'');
            var name = document.getElementById('realname-' + lid);
            name.innerHTML = user.result.users[i].real_name;
            }
        }
      });
    });
    
    $.getJSON('https://git.reviewboard.kde.org/api/review-requests/?to-group=marble&callback=?',
    {
      "to-groups": "marble",
      "max-results": 50,
      "status": "pending"
    },
    function(review) {
      var table = document.getElementById('table-reviews');
      document.getElementById('review-status').innerHTML = "Currently there are " + review.review_requests.length + " open reviews. Color indicates the age of the last change.";
      for (var i=0;i<review.review_requests.length;i++)
      {
        var row = table.insertRow(table.rows.length);

        const one_day=1000*60*60*24;
        var lastChange = new Date(review.review_requests[i].last_updated);
        var age = new Date().getTime() - lastChange.getTime();
        var ageDays = age / one_day;
        if (ageDays > 31.0) {
          row.setAttribute('class', 'error');
        } else if (ageDays > 7.0) {
          row.setAttribute('class', 'warning');
        } else {
          row.setAttribute('class', 'success');
        }

        // Summary
        row.insertCell(0).innerHTML = review.review_requests[i].summary;

        // Last changed
        var lastChange = new Date(review.review_requests[i].last_updated);
        row.insertCell(1).innerHTML = fuzzyDate(lastChange);

        // Review board link
        var button = document.createElement("button");
        button.setAttribute("type", 'button');
        button.setAttribute("class", 'btn btn-default btn-sm');
        var link = document.createElement("a");
        link.href = 'https://git.reviewboard.kde.org/r/' + review.review_requests[i].id;
        link.innerHTML = '#' + review.review_requests[i].id;

        button.appendChild(link);
        row.insertCell(2).appendChild(button);
      }
     }
    );
  /* ]]> */
  </script>

  </body>
</html>
