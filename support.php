<!DOCTYPE html>
<html lang="en">
<?php include "head.inc"; ?>
<body>
<?php include "navigation.inc"; ?>
  <div class="container">
  
  <h3>Community Support</h3>
  <p>Marble has got an outstanding community: We try to answer all questions and try to help everybody voluntarily. So our community support is free of charge &mdash; just like the entire Marble source code is available as open source!</p>
  
  <div class="col-md-12">
    <div class="thumbnail">
    <div class="caption">
      <h3>Documentation <small>some support questions are covered here already</small></h3>
    <p><a class="btn btn-default" href="http://docs.kde.org/development/en/kdeedu/marble/"><i class="icon-book"></i> User Manual</a>
    <a href="http://userbase.kde.org/Tutorials#Marble" class="btn btn-default"><i class="icon-globe"></i> Tutorials</a>
    <a class="btn btn-default" href="http://userbase.kde.org/Marble"><i class="icon-globe"></i> UserBase</a>
    <a class="btn btn-default" href="https://techbase.kde.org/Marble/FAQ"><i class="icon-globe"></i> FAQ</a></p>
    </div>
    </div>
  </div>
  
  <div class="col-md-6">
    <div class="thumbnail">
    <div class="caption">
      <h3>Help</h3>
    <p>End-users and developers use the Marble forum for support questions and to discuss suggestions.<br />
      <a class="btn btn-default" href="http://forum.kde.org/viewforum.php?f=217"><i class="icon-comment"></i> Marble Forum</a>
    </p>
    
    <p>There are special support threads for the mobile Marble versions.<br />
    <a class="btn btn-default" href="http://talk.maemo.org/showthread.php?t=67316"><i class="icon-globe"></i> N900 Support</a>
    <a class="btn btn-default" href="http://talk.maemo.org/showthread.php?t=82196"><i class="icon-globe"></i> N9/N950 Support</a></p>
    
    <p>Many Marble developers and users are regular visitors of the <tt>#marble</tt> IRC channel in the freenode network. This is where most of the development discussion happens, and where you can find <abbr title="Often questions are answered within minutes, but people might not respond immediately. Please wait up to some hours for a reply, and raise your question again or come back later if needed.">quick</abbr> answers to your questions.<br />
      <a class="btn btn-default" href="irc://irc.freenode.net/marble"><i class="icon-user"></i> IRC</a>
    </p>
    
    <p>To reach everybody involved in the Marble project use the marble-devel mailing list. Ideal for announcements and bigger/longer discussions, but also suited for support questions.<br />
      <a class="btn btn-default" href="https://mail.kde.org/mailman/listinfo/marble-devel"><i class="icon-envelope"></i> Mailing list</a>
    </p>

    </div>
    </div>
  </div>
  
  <div class="col-md-6">
    <div class="thumbnail">
    <div class="caption">
      <h3>Bugs and Suggestions</h3>
    <p>If you encounter a crash, misbehavior or similar malfunction in one of the Marble applications, please report a bug.<br />
    <a class="btn btn-default" href="https://bugs.kde.org/enter_bug.cgi?product=marble&component=general&bug_severity=normal"><i class="icon-globe"></i> File a Bug</a></p>
    
    <p>Got a suggestion to improve usability? Are you missing a certain feature? Please report a suggestion (a bug with severity set to <i>wishlist</i>).<br />
    <a class="btn btn-default" href="https://bugs.kde.org/enter_bug.cgi?product=marble&component=general&bug_severity=wishlist"><i class="icon-globe"></i> File a Suggestion</a></p>
    
    <p>You can also view the list of already existing bugs and suggestions. Don't worry about filing duplicates though, we'll sort that out. In doubt, please file a bug/suggestion.</p>
    <p><a class="btn btn-default" href="https://bugs.kde.org/buglist.cgi?order=Bug%20Number&bug_severity=critical&bug_severity=grave&bug_severity=major&bug_severity=crash&bug_severity=normal&bug_severity=minor&query_format=advanced&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&product=marble"><i class="icon-globe"></i> Open Bugs</a>
    <a class="btn btn-default" href="https://bugs.kde.org/buglist.cgi?order=Bug%20Number&bug_severity=wishlist&bug_severity=task&query_format=advanced&bug_status=UNCONFIRMED&bug_status=CONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&product=marble"><i class="icon-globe"></i> Open Suggestions</a></p>
    </div>
    </div>
  </div>

  <div class="clearfix visible"></div>
  <h3>Commercial Support</h3>
  
  <ul>
    <li>You'd like to have Marble extended with some custom feature? And you need it quickly in time?</li>
    <li>You want to develop a custom app based on Marble and you need development support?</li>
    <li>You'd like to get Qt and Marble programming training?</li>
  </ul>

<p>There are several companies who provide commercial support for Marble. Here's a list of companies which provide commercial support for Marble and which have a <i>track-record for Marble development</i>:</p>

  <table class="table table-bordered">
      <tr>
          <th>Company logo</th> <!--logo-->
          <th>Name/Website</th> <!--company name-->
          <th>Contact</th> <!--contact-->
          <th>Support options</th> <!--support options-->
      </tr>
      <tr>
          <td><a href="https://www.basyskom.com"><img border="0" src="img/icons/basysKom_Logo_rgb.png" /></a></td> <!--logo-->
          <td><a href="https://www.basyskom.com">basysKom GmbH</a></td> <!--company name-->
          <td><a href="mailto:info@basyskom.de">info@basyskom.de</a></td> <!--contact-->
          <td>Custom Marble solutions, training</td> <!--support options-->
      </tr>
      <tr>
          <td><a href="http://www.c-xx.com"><img border="0" src="img/icons/c-xx.com-logo.png" /></a></td> <!--logo-->
          <td><a href="http://www.c-xx.com">c-xx.com / Hoffmann Information Services GmbH</a></td> <!--company name-->
          <td><a href="mailto:mail@c-xx.com">mail@c-xx.com</a></td> <!--contact-->
          <td>Custom Marble solutions, training</td> <!--support options-->
      </tr>
  </table>

<p>Companies who are providing support for Marble are encouraged to <i>contribute back</i> to the Marble project if possible. This helps Marble to stay a competitive solution. And it helps to build further projects on top.</p>
<p>If your company offers commercial support for Marble and if you'd like your company name added to this page please contact <a href="mailto:marble-devel@kde.org">marble-devel@kde.org</a>.</p>
   
  </div>
  <!-- /container -->

  <?php include "footer.inc"; ?>

  </body>
</html>
