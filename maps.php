<!DOCTYPE html>
<html lang="en">
<?php include "head.inc"; ?>
<body>
<?php include "navigation.inc"; ?>
  <div class="container">

  <h3>Additional Maps</h3>
  <p>This page contains a few packages that contain map data for the Qt version of Marble. You can install this data manually. Users of the KDE version can get these maps with <tt>Download maps...</tt> in the <tt>File</tt> menu You have to extract these files into Marble's maps directory. On Windows this is <code>data/maps</code> in your Marble installation. On Linux you will find it in <code>$XDG_DATA_HOME/marble/maps</code> while <code>$XDG_DATA_HOME</code> is usually <code>~/.local/share</code>.</p>
  
  <p>Here's an example for Linux and the <i>Historical Map 1570</i>: Download the map theme using the download link in the table below, saving the file as <tt>ortelius1570-marblemap.zip</tt>. Open a shell and run</p>
  <pre>unzip -d ~/.local/share/marble/maps ortelius1570-marblemap.zip</pre>
  <p>Now you just need to run Marble and select the newly installed map theme.</p>

<?php  
$xsl = new DOMDocument();
$xsl->load("maps.xsl");

$xml = new DOMDocument();
$xml->load("https://edu.kde.org/marble/newstuff/maps.xml");

$proc = new XsltProcessor();
$proc->importStylesheet($xsl);
// For windows compliance we also provide .zip files that are not mentioned
// in the .xml file (and should not be used there).
echo str_replace(".tar.gz", ".zip", $proc->transformToXML($xml));
?>

  </div>
  <!-- /container -->

  <?php include "footer.inc"; ?>

  </body>
</html>
