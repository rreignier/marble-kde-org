<!DOCTYPE html>
<html lang="en">
<?php include "head.inc"; ?>
<body>
<?php include "navigation.inc"; ?>
  <div class="container">    
    
    <div class="tabbable tabs-right">
    
      <ul id="codeTab" class="nav nav-tabs">
        <li><a href="changelog.php">Marble 1.5 ...</a></li>
        <li class="active"><a href="#v14" data-toggle="tab">Marble 1.4</a></li>
        <li><a href="#v13" data-toggle="tab">Marble 1.3</a></li>
        <li><a href="#v12" data-toggle="tab">Marble 1.2</a></li>
        <li><a href="#v11" data-toggle="tab">Marble 1.1</a></li>
        <li><a href="#v10" data-toggle="tab">Marble 1.0</a></li>
        <li><a href="#v09" data-toggle="tab">Marble 0.10</a></li>
        <li><a href="#v09" data-toggle="tab">Marble 0.9</a></li>
        <li><a href="#v08" data-toggle="tab">Marble 0.8</a></li>
        <li><a href="#v07" data-toggle="tab">Marble 0.7</a></li>
        <li><a href="#v06" data-toggle="tab">Marble 0.6</a></li>
      </ul>
  
      <div id="codeTabContent" class="tab-content">
        <div class="tab-pane fade in active" id="v14"><?php include "changelogs/1.4.inc"; ?></div>
        <div class="tab-pane fade" id="v13"><?php include "changelogs/1.3.inc"; ?></div>
        <div class="tab-pane fade" id="v12"><?php include "changelogs/1.2.inc"; ?></div>
        <div class="tab-pane fade" id="v11"><?php include "changelogs/1.1.inc"; ?></div>
        <div class="tab-pane fade" id="v10"><?php include "changelogs/1.0.inc"; ?></div>
        <div class="tab-pane fade" id="v09"><?php include "changelogs/0.10.inc"; ?></div>
        <div class="tab-pane fade" id="v09"><?php include "changelogs/0.9.inc"; ?></div>
        <div class="tab-pane fade" id="v08"><?php include "changelogs/0.8.inc"; ?></div>
        <div class="tab-pane fade" id="v07"><?php include "changelogs/0.7.inc"; ?></div>
        <div class="tab-pane fade" id="v06"><?php include "changelogs/0.6.inc"; ?></div>
      </div>
    </div> <!-- /tabbable -->
    
  </div> <!-- /container -->

  <?php include "footer.inc"; ?>

  <script>
  var $ = jQuery.noConflict();
  $(document).ready(function() {
    if (location.hash !== '') $('a[href="' + location.hash + '"]').tab('show');
    return $('a[data-toggle="tab"]').on('shown', function(e) {
      return location.hash = $(e.target).attr('href').substr(1);
    });
  });
  </script>

  </body>
</html>
