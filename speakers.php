<!DOCTYPE html>
<html lang="en">
<?php include "head.inc"; ?>
<body>
<?php include "navigation.inc"; ?>
  <div class="container">

<div class="row">
<div class="col-md-4">
<img src="img/screenshots/1.2/Voice-of-marble.png" />
</div>
<div class="col-md-8">
<h3>Voice Guidance Speakers</h3>
<p>
Here you'll find additional voice guidance speakers for download.
</p>

<p>
It's still possible to add more speakers! Raise your voice and contribute a speaker file in your native language. See the <a href="https://community.kde.org/Marble/VoiceOfMarble">Voice Of Marble</a> website in the KDE Community wiki for details.
</p>
</div>
</div>

<?php
$xsl = new DOMDocument();
$xsl->load("speakers.xsl");

$xml = new DOMDocument();
$xml->load("https://edu.kde.org/marble/newstuff/speakers.xml");

$proc = new XsltProcessor();
$proc->importStylesheet($xsl);
echo $proc->transformToXML($xml);
?>

  </div>
  <!-- /container -->

  <?php include "footer.inc"; ?>

  </body>
</html>
