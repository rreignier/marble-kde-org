<!DOCTYPE html>
<html lang="en">
<?php include "head.inc"; ?>
<body>
<?php include "navigation.inc"; ?>
  <div class="container">
  
  <p>
  We are a vibrant community of map enthusiasts who work to ensure freedom for all people through our software. We are part of KDE and this is our emphasis of the <a href="http://manifesto.kde.org/">KDE Manifesto</a>:
  </p>
  
  <div class="col-md-8">  
  <div class="row">
  <div class="col-md-6">
    <h1>Vision</h1>
    <p class="lead">
    We want Marble to be your swiss army knife for maps.
    </p>
  </div>
  <div class="col-md-6">
    <h1>Motto</h1>
    <p class="lead">
    Find your way and explore the world!
    </p>
  </div>
  <div class="col-md-6">
    <h1>Mission</h1>
    <p class="lead">
    We seek to create a simple and clean, task-oriented virtual globe and world atlas software for casual users. While feature-rich and flexible, we design it to be easy to use and visually pleasing. We provide a light-weight, easy to embed library for developers that is fast and works cross-platform with minimal hardware and software requirements.
    </p>
  </div>
  <div class="col-md-6">
    <h1>Core Values</h1>
    <p class="lead">
    We endorse free software, open standards, free maps and free data. Proprietary webservices shall not be a requirement. We prefer solutions that work offline. Everyone is invited to join our community. We participate as mentors in several programs to introduce students to the world of <abbr title="Free and open-source software">FOSS</abbr>.
    </p>
  </div>
  </div>
  </div>
  
  <div class="col-md-4">
    <iframe src="//embed.gettyimages.com/embed/157339411?et=1Ws_utvt0E6wDd_r3YP1UQ&sig=Z4HcfSdDuJ9yoiVQpKX731YYLAKwIjTWvDjn752t_Kg=" width="414" height="483" frameborder="0" scrolling="no"></iframe>
  </div>

  </div>
  <!-- /container -->
  
  <?php include "footer.inc"; ?>

  </body>
</html>
