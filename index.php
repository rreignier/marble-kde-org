<!DOCTYPE html>
<html lang="en">
<?php include "head.inc"; ?>
<body>
<?php include "navigation.inc"; ?>
  <div class="container">

    <!--
    <div class="alert alert-info" style="margin: 5px 0; margin-bottom: 0px;">
      <strong>Latest News</strong>
      <ul>
ch        <li><a href="install.php">Marble 2.2 has been released</a> as part of <a href="https://www.kde.org/announcements/announce-applications-17.04.0.php">KDE Applications 17.04</a>!</li>
        <li>A beautiful OpenStreetMap: Install <a href="https://play.google.com/store/apps/details?id=org.kde.marble.maps">Marble Maps</a> on your Android devices.</li>
      </ul>
    </div>
    -->

    <!-- Main hero unit for a primary marketing message or call to action -->
    <div class="hero-unit">
    
      <div class="row" style="padding-top:20px"></div>
      
      <div class="row">
	<div class="col-md-1"></div>

	<div class="col-md-5">
	    <div>
	    <center>
		<img src="img/gallery/marble-desktop-atlas-thinkpad_400.jpg" class="img-rounded"/>
	    </center>
	    </div>
	</div>
	
	<div class="col-md-5">
	    <center>
	  <img src="img/marble-logo.png" style="padding-left:80px; padding-right:80px" class="img-responsive" />
	  <p class="lead">Find your way and explore the world!</p>
	    
    	  <p>Marble is a virtual globe and world atlas &mdash; your swiss army knife for maps.<br /></p>
	    </center>

	</div>
	
	</p>
	
      </div>
      
      <div class="row" style="padding-top:80px"></div>

      <div class="row">
	<div class="col-md-1"></div>
	
	<div class="col-md-4">
	  <h2>Desktop</h2>
	  <p>Versatile, yet easy to use. Use Marble similar to a desktop globe; pan around and measure distances. At closer scale it becomes a world atlas, while OpenStreetMap takes you to street level. Search for places of interest, view Wikipedia articles, create routes by drag and drop and so much more.</p>
	  <div style="float:right; margin-bottom:10px; margin-right:10px;"><span class="label label-info">Linux, Mac, Windows</span></div>
	  <p><a href="features.php"><button type="button" class="btn btn-default">View Details &raquo;</button></a></p>
	</div>
	
	<div class="col-md-6">
	    <div>
	    <center>
		<img src="img/gallery/marble-desktop-sentinel_400.png" class="img-responsive"/>
	    </center>
	    </div>
	</div>
      </div>
      
      <div class="row">
	<div class="col-md-1"></div>
	
	<div class="col-md-4">
	  <h2>Mobile</h2>
	  <p><a href="https://play.google.com/store/apps/details?id=org.kde.marble.maps">Marble Maps</a> brings the highly detailed OpenStreetMap to your mobile devices. It features a crisp, beautiful map with an intuitive user interface.</p>
	  <p>The <a href="https://play.google.com/store/apps/details?id=org.kde.marble.behaim">Behaim Globe</a> is a digital reproduction of a globe crafted at the time of Columbus' first sea travel to the west.</p>
	  <div style="float:right; margin-bottom:10px; margin-right:10px;"><span class="label label-info">Android</span></div>
	  <p><a href="features.php"><button type="button" class="btn btn-default">View Details &raquo;</button></a></p>
	</div>
	
	<div class="col-md-6">
	    <div>
	    <center>
		<img src="img/gallery/marble-android-devices_400.png" class="img-responsive"/>
	    </center>
	    </div>
	</div>
	
      </div>
	
      <div class="row">
	<div class="col-md-1"></div>
	
	<div class="col-md-10">
	  <img align="right" alt="worker icon" src="img/icons/worker.png" style="margin:10px; margin-top:15px; vertical-align: bottom;" />
	  <h2>Extensible</h2>
	  <p>At its heart, a software library powers the Marble applications -- also integrated in a wide variety of other projects like <abbr title="Your favorite photo management software uses Marble for georeferencing photos">Digikam</abbr> or the <abbr title="Your favorite desktop environment has a Marble wallpaper and a world clock plasmoid">KDE workspaces</abbr>. Using the Marble library in your project requires only C++, Qt and adherence to the <abbr title="GNU Lesser General Public License, Version 2.1">LGPL 2.1</abbr>. Last but not least you can extend Marble itself using plugins.</p>
	  <p><a href="dev-intro.php"><button type="button" class="btn btn-default">View Details &raquo;</button></a></p>
	</div>
	
      </div>

    <!-- /container -->
    </div>
    <?php include "footer.inc"; ?>

<script type="text/javascript">
      var $ = jQuery.noConflict();
      $(document).ready(function() {
      $('#myCarousel').carousel({
          interval: 5000,
          cycle: true
        });
      });
</script>


  </body>
</html>
