<!-- Fixed navbar -->
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php">Marble</a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
          <li><a href="features.php">Features</a></li>
          <!--<li><a href="changelog.php">Changelog</a></li>-->
          <li><a href="documentation.php">Documentation</a></li>
          <li><a href="support.php">Support</a></li>
          <li><a href="blog.php">Blog</a></li>
          <li><form action="install.php"><button type="submit" class="btn btn-success navbar-btn" style="padding-top:3pt; padding-bottom:3pt">Install</button></form></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Developers <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li class="nav-header disabled"><a>The Marble Library</a></li>
              <li><a href="dev-intro.php">Getting Started</a></li>
              <li><a href="sources.php">Source Code</a></li>
              <li><a href="packaging.php">Build Options</a></li>
              <li><a href="dev-doc.php">Documentation</a></li>
              <!--<li><a href="kml-guide.php">KML Guide</a></li>-->
              <li class="divider">Marble Development</li>
              <li class="nav-header disabled"><a>Marble Development</a></li>
              <li><a href="contributors-welcome.php">Welcome, Contributors!</a></li>
              <!--<li><a href="dashboard.php">Task Dashboard</a></li>-->
            </ul>
          </li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</div>
    
