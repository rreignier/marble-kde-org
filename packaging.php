<!DOCTYPE html>
<html lang="en">
<?php include "head.inc"; ?>
<body>
<?php include "navigation.inc"; ?>
  <div class="container">
  Trying to package Marble? We take special care to minimize the number of dependencies Marble needs to be built &mdash; it's only Qt. Combined with cmake as build system, compiling Marble should be a breeze. Still there are a lot of options to tweak things as needed. This page attempts to list them.
  
  <h3>Dependencies</h3>
  <p>When configuring Marble with <tt>cmake</tt>, dependencies are automatically detected and configured. A working compiler and Qt development packages are required; otherwise cmake will abort with an error. If (for whatever reason) you want to deactivate a package that was found, use the <code>WITH_$pkg=OFF</code> switch like shown in the table below.</p>
  
  <table class="table table-striped table-bordered table-hover table-condensed">
  <tr>
   <th>Dependency</th>
   <th>Type</th>
   <th>Description</th>
   <th>Deactivation</th>
  </tr>
  <tr>
   <td>Qt 5</td>
   <td><span class="label label-success">Required</span></td>
   <td>the famous cross-platform application framework</td>
   <td></td>
  </tr>
  <tr>
   <td>KDE Frameworks 5</td>
   <td><span class="label label-info">Optional</span></td>
   <td>collection of libraries and software frameworks by KDE extending the Qt framework</td>
   <td><code>WITH_KF5=OFF</code></td>
  </tr>
  <tr>
   <td>libshp</td>
   <td><span class="label label-info">Optional</span></td>
   <td>reading and displaying .shp files</td>
   <td><code>WITH_libshp=OFF</code></td>
  </tr>
  <tr>
   <td>libgps</td>
   <td><span class="label label-info">Optional</span></td>
   <td>position information via gpsd</td>
   <td><code>WITH_libgps=OFF</code></td>
  </tr>
  <tr>
   <td>libwlocate</td>
   <td><span class="label label-info">Optional</span></td>
   <td>Position information based on neighboring WLAN networks</td>
   <td><code>WITH_libwlocate=OFF</code></td>
  </tr>
  </table>
   
  <h3>Build Options</h3>
  <p>There are other cmake switches than those shown above to control dependencies.</p>
  
    <table class="table table-striped table-bordered table-hover table-condensed">
  <tr>
   <th>Switch</th>
   <th>Description</th>
   <th>Default</th>
  </tr>
  <tr>
   <td><code>BUILD_MARBLE_TESTS</code></td>
   <td>Build test cases and test suite</td>
   <td><code>OFF</code></td>
  </tr>
  <tr>
   <td><code>BUILD_MARBLE_TOOLS</code></td>
   <td>Build various tools related to Marble</td>
   <td><code>OFF</code></td>
  </tr>
  <tr>
   <td><code>BUILD_MARBLE_EXAMPLES</code></td>
   <td>Build C++ examples showing how to use the Marble library</td>
   <td><code>OFF</code></td>
  </tr>
  <tr>
   <td><code>MARBLE_PRI_INSTALL_USE_QT_SYS_PATHS</code></td>
   <td>Install the Marble pri file for qmake into the Qt system directories</td>
   <td><code>OFF</code><br />(if installed to different prefix)</td>
  </tr>
  </table>
  
   
  </div>
  <!-- /container -->

  <?php include "footer.inc"; ?>

  </body>
</html>
