<!DOCTYPE html>
<html lang="en">
<?php include "head.inc"; ?>
<body>
<link href="css/prettify.css" type="text/css" rel="stylesheet" />
<script type="text/javascript" src="js/prettify.js"></script>
<link rel="stylesheet" href="https://cdn.leafletjs.com/leaflet-0.4/leaflet.css" />
<link rel="stylesheet" href="leaflet-locatecontrol/L.Control.Locate.css" />
 <!--[if lte IE 8]>
     <link rel="stylesheet" href="https://cdn.leafletjs.com/leaflet-0.4/leaflet.ie.css" />
     <link rel="stylesheet" href="leaflet-locatecontrol/L.Control.Locate.ie.css" />
 <![endif]-->
<script src="https://cdn.leafletjs.com/leaflet-0.4/leaflet.js"></script>
<script type="text/javascript" src="js/kml-guide.js"></script>
<script type="text/javascript" src="leaflet-locatecontrol/L.Control.Locate.js"></script>
<?php include "navigation.inc"; ?>
  <div class="container">

  <p><abbr title="Keyhole Markup Language">KML</abbr> is an XML format for geographic annotations developed by Keyhole, Inc. for their Keyhole Earth Viewer. The latter was renamed Google Earth after the eponymous company acquired Keyhole, Inc. in 2004. Since 2008 KML is an international standard of the <abbr title="Open Geospatial Consortium">OGC</abbr>. Marble makes extensive use of KML both in internal representations and for serialization (reading and writing) of data.</p>

  <p>This page assumes a basic knowledge of KML and concentrates on Marble specific aspects. Please refer to the <a href="https://developers.google.com/kml/documentation/" target="_blank">KML Documentation</a> at Google Developers for an introduction to KML and a complete reference.</p>

  <script type="text/javascript">
    function setVersion(version)
    {
      elements = [ 'Polygon-write', 'ScreenOverlay-read', 'ScreenOverlay-write', 'PhotoOverlay-write', 'GroundOverlay-write', 'ScreenOverlay-use', 'TimeSpan-write', 'BalloonStyle-read', 'BalloonStyle-write', 'ListStyle-read', 'ListStyle-write', 'NetworkLink-read', 'NetworkLink-write', 'gx:Tour-read', 'Region-write', 'Camera-read', 'Camera-write', 'gx:TimeSpan-read', 'gx:TimeStamp-read', 'Model-read' ];
      if (version === 15)
      {
        for (var i=0; i<elements.length; ++i) {
          document.getElementById(elements[i]).innerHTML = "<i class=\"icon-minus-sign\"></i>";
          document.getElementById(elements[i]).setAttribute("class", "no");
        }
        document.getElementById('LineStyle-use').innerHTML = "<i class=\"icon-ok\"></i> <a style=\"cursor:pointer\" rel=\"popover\" data-placement=\"top\" data-content=\"No support for random colors\"><i class=\"icon-question-sign\"></i></a>";
        document.getElementById('PolyStyle-use').innerHTML = "<i class=\"icon-ok\"></i> <a style=\"cursor:pointer\" rel=\"popover\" data-placement=\"top\" data-content=\"No support for random colors\"><i class=\"icon-question-sign\"></i></a>";
        document.getElementById('IconStyle-use').innerHTML = "<i class=\"icon-ok\"></i> <a style=\"cursor:pointer\" rel=\"popover\" data-placement=\"top\" data-content=\"No support for remote icons and random colors\"><i class=\"icon-question-sign\"></i></a>";
        document.getElementById('LabelStyle-use').innerHTML = "<i class=\"icon-ok\"></i> <a style=\"cursor:pointer\" rel=\"popover\" data-placement=\"top\" data-content=\"No support for random colors\"><i class=\"icon-question-sign\"></i></a>";
        $("a[rel=popover]").popover();
      }
      else if (version === 16)
      {
        for (var i=0; i<elements.length; ++i) {
          document.getElementById(elements[i]).innerHTML = "<i class=\"icon-ok\"></i>";
          document.getElementById(elements[i]).setAttribute("class", "yes");
        }
        document.getElementById('LineStyle-use').innerHTML = "<i class=\"icon-ok\"></i>";
        document.getElementById('PolyStyle-use').innerHTML = "<i class=\"icon-ok\"></i>";
        document.getElementById('IconStyle-use').innerHTML = "<i class=\"icon-ok\"></i> <a style=\"cursor:pointer\" rel=\"popover\" data-placement=\"top\" data-content=\"No support for remote icons\"><i class=\"icon-question-sign\"></i></a>";
        document.getElementById('LabelStyle-use').innerHTML = "<i class=\"icon-ok\"></i>";
        $("a[rel=popover]").popover();
      }
    }
  </script>

  <h3>KML Support in Marble</h3>
    The following table summarizes Marble's support for KML serialization and display in Marble
    <div class="btn-group" data-toggle="buttons">
     <label class="btn btn-default btn-sm active" onClick='setVersion(15);'><input type="radio">1.5</input></label>
     <label class="btn btn-default btn-sm" onClick='setVersion(16);'><input type="radio">1.6</input></label>
    </div>

  <table class="table table-striped table-bordered table-hover table-condensed">

<?php

function print_cell($style, $desc, $hint, $myid)
{
  print "<td id=\"" . $myid . "\" class=\"" .$style . "\">";
  if ($hint != "") {
    print "<a style=\"cursor:pointer\" rel=\"popover\" data-placement=\"top\" data-content=\"" . $hint . "\">";
  }

  if ($desc == "yes") {
    print "<i class=\"icon-ok\"></i>\n";
  } elseif ($desc == "+") {
    print "<i class=\"icon-ok\"></i>\n";
  } elseif ($desc == "-") {
    print "<i class=\"icon-minus-sign\"></i>\n";
  } elseif ($desc == "?") {
    print "<i class=\"icon-question-sign\"></i>\n";
  } else {
    print $desc;
  }
  if ($hint != "") {
    print "<i class=\"icon-info-sign\" style=\"margin-left:20px;\"></i></a>\n";
  }
  print "</td>\n";
}

function print_row($title, $description, $kde_style, $kde_desc, $kde_hint, $qt_style, $qt_desc, $qt_hint, $n900_style, $n900_desc, $n900_hint)
{
  print "<tr>\n";
  print "<th><a style=\"cursor:pointer\" rel=\"popover\" data-placement=\"right\" title=\"" . $title . "\" ";
  print "data-content=\"" . $description . "\">" . $title . "</a></th>\n";
  print_cell($kde_style, $kde_desc, $kde_hint, $title . "-read");
  print_cell($qt_style, $qt_desc, $qt_hint, $title . "-use");
  print_cell($n900_style, $n900_desc, $n900_hint, $title . "-write");

  print "</tr>\n";
}

function print_head_row($title)
{
  //print "<tr>\n<th colspan=\"6\">" . $title . "</th>\n</tr>\n";
  print "<tr>\n<th style=\"padding-top:15px\">" . $title . "</th>\n";
  print "<th style=\"padding-top:15px\">Read</th>\n";
  print "<th style=\"padding-top:15px\">Display/Use</th>\n";
  print "<th style=\"padding-top:15px\">Write</th>\n</tr>\n";
}

  print_head_row("Features");
  print_row("Document", "Document",
     "yes", "yes", "",
     "yes", "yes", "",
     "yes", "yes", "");
  print_row("Folder", "Folder",
     "yes", "yes", "",
     "yes", "yes", "",
     "yes", "yes", "");
  print_row("Placemark", "Placemark",
     "yes", "yes", "",
     "yes", "yes", "",
     "yes", "yes", "");
  print_row("NetworkLink", "NetworkLink",
     "no", "-", "",
     "no", "-", "",
     "no", "-", "");
  print_row("gx:Tour", "gx:Tour",
     "no", "-", "",
     "no", "-", "",
     "no", "-", "");

  print_head_row("Geometries");
  print_row("Point", "Point",
     "yes", "yes", "",
     "yes", "yes", "",
     "yes", "yes", "");
  print_row("LineString", "LineString",
     "yes", "yes", "",
     "yes", "yes", "",
     "yes", "yes", "");
  print_row("LinearRing", "LinearRing",
     "yes", "yes", "",
     "yes", "yes", "",
     "yes", "yes", "");
  print_row("Polygon", "Polygon",
     "yes", "yes", "",
     "yes", "yes", "",
     "no", "-", "");
  print_row("MultiGeometry", "MultiGeometry",
     "yes", "yes", "",
     "yes", "yes", "",
     "yes", "yes", "");
  print_row("Model", "Model",
     "no", "-", "",
     "no", "-", "",
     "no", "-", "");
  print_row("Region", "Region",
     "yes", "yes", "",
     "", "?", "",
     "no", "-", "");
  print_row("gx:Track", "gx:Track",
     "yes", "yes", "",
     "yes", "yes", "",
     "yes", "yes", "Not all elements are written yet");
  print_row("gx:MultiTrack", "gx:MultiTrack",
     "yes", "yes", "",
     "yes", "yes", "",
     "yes", "yes", "Not all elements are written yet");

  print_head_row("Overlays");
  print_row("PhotoOverlay", "PhotoOverlay",
     "yes", "yes", "",
     "opt", "+", "rotation is ignored",
     "no", "-", "");
  print_row("ScreenOverlay", "ScreenOverlay",
     "no", "-", "",
     "no", "-", "",
     "no", "-", "");
  print_row("GroundOverlay", "GroundOverlay",
     "yes", "yes", "",
     "opt", "+", "Not displayed in spherical projection",
     "no", "-", "");

  print_head_row("Time");
    print_row("TimeSpan", "TimeSpan",
     "yes", "yes", "",
     "no", "-", "",
     "no", "-", "");
  print_row("TimeStamp", "TimeStamp",
     "yes", "yes", "",
     "no", "-", "",
     "yes", "yes", "");
  print_row("gx:TimeSpan", "gx:TimeSpan",
     "no", "-", "",
     "no", "-", "",
     "no", "-", "");
  print_row("gx:TimeStamp", "gx:TimeStamp",
     "no", "-", "",
     "no", "-", "",
     "no", "-", "");

  print_head_row("Styles");
  print_row("Style", "Style",
     "yes", "yes", "",
     "yes", "yes", "",
     "yes", "yes", "");
  print_row("StyleMap", "StyleMap",
     "yes", "yes", "",
     "yes", "yes", "",
     "yes", "yes", "");
  print_row("BalloonStyle", "BalloonStyle",
     "no", "-", "",
     "no", "-", "",
     "no", "-", "");
  print_row("ListStyle", "ListStyle",
     "no", "-", "",
     "no", "-", "",
     "no", "-", "");
  print_row("LineStyle", "LineStyle",
     "yes", "yes", "",
     "yes", "yes", "Random colors are not supported",
     "yes", "yes", "");
  print_row("PolyStyle", "PolyStyle",
     "yes", "yes", "",
     "yes", "yes", "Random colors are not supported",
     "yes", "yes", "");
  print_row("IconStyle", "IconStyle",
     "yes", "yes", "",
     "opt", "+", "No support for remote icons and random colors",
     "yes", "yes", "");
  print_row("LabelStyle", "LabelStyle",
     "yes", "yes", "",
     "yes", "yes", "Random colors are not supported",
     "yes", "yes", "");

  print_head_row("Views");
  print_row("Camera", "Camera",
     "no", "-", "",
     "", "?", "",
     "no", "-", "");
  print_row("LookAt", "LookAt",
     "yes", "yes", "",
     "", "?", "",
     "yes", "yes", "");

?>

  </table>

  <h3>KML Generator</h3>
  <p>Generate <tt>.kml</tt> files with placemarks directly from points of interest stored in OpenStreetMap: Change the map region and the point of interest type in the dropdown list as you like and press the <i>update data</i> button. The KML contents appear in the text box; you can download them as a <tt>.kml</tt> file and open it in Marble.</p>

  <div id="size-warning" class="collapse out">
    <div class="alert alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <h4>Large region selected</h4>
    <p>The current map region is large. There's nothing wrong with that, but your browser may become slow for some time while handling the data if you press <tt>update data</tt> now. Zoom in further to avoid that.</p>
    </div>
  </div>

  <input type="hidden" id="osmfeature" value="amenity=ice_cream" />
  <div class="row">
  <div class="col-md-6" style="height:400px;">

    Find
    <div class="btn-group">
      <button class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
        <span id="current-poi-type"><img width="12" height="12" src="img/icons/map/food_ice_cream.p.12.png" /> ice cream</span>
        <span class="caret"></span>
      </button>
      <ul class="dropdown-menu" role="menu">
          <li><img width="12" height="12" src="img/icons/map/food_cafe.p.12.png" /> Amenity</li>
          <li><a onClick='store("amenity=bar")'><img width="12" height="12" src="img/icons/map/food_bar.p.12.png" /> bars</a></li>
          <li><a onClick='store("amenity=cafe")'><img width="12" height="12" src="img/icons/map/food_cafe.p.12.png" /> cafes</a></li>
          <li><a onClick='store("amenity=ice_cream")'><img width="12" height="12" src="img/icons/map/food_ice_cream.p.12.png" /> ice cream</a></li>
          <li><a onClick='store("leisure=playground")'><img width="12" height="12" src="img/icons/map/amenity_playground.p.12.png" /> playgrounds</a></li>
          <li><a onClick='store("amenity=pub")'><img width="12" height="12" src="img/icons/map/food_pub.p.12.png" /> pubs</a></li>
          <li><a onClick='store("amenity=restaurant")'><img width="12" height="12" src="img/icons/map/food_restaurant.p.12.png" /> restaurants</a></li>
          
          <li><img width="12" height="12" src="img/icons/map/education_university.p.12.png" /> Education</li>
          <li><a onClick='store("amenity=college")'><img width="12" height="12" src="img/icons/map/education_college.p.12.png" /> colleges</a></li>
          <li><a onClick='store("amenity=library")'><img width="12" height="12" src="img/icons/map/amenity_library.p.12.png" /> libraries</a></li>
          <li><a onClick='store("amenity=school")'><img width="12" height="12" src="img/icons/map/education_school.p.12.png" /> schools</a></li>
          <li><a onClick='store("amenity=university")'><img width="12" height="12" src="img/icons/map/education_university.p.12.png" /> universities</a></li>
          
          <li><img width="12" height="12" src="img/icons/map/tourist_theatre.p.12.png" /> Entertainment</a></li>
          <li><a onClick='store("tourism=attraction")'><img width="12" height="12" src="img/icons/map/tourist_attraction.p.12.png" /> attractions</a></li>
          <li><a onClick='store("historic=castle")'><img width="12" height="12" src="img/icons/map/tourist_castle2.p.12.png" /> castles</a></li>
          <li><a onClick='store("amenity=cinema")'><img width="12" height="12" src="img/icons/map/tourist_cinema2.p.12.png" /> cinemas</a></li>
          <li><a onClick='store("tourism=museum")'><img width="12" height="12" src="img/icons/map/tourist_museum.p.12.png" /> museums</a></li>
          <li><a onClick='store("amenity=theatre")'><img width="12" height="12" src="img/icons/map/tourist_theatre.p.12.png" /> theatres</a></li>
          <li><a onClick='store("tourism=viewpoint")'><img width="12" height="12" src="img/icons/map/tourist_view_point.p.12.png" /> viewpoints</a></li>
          <li><a onClick='store("tourism=zoo")'><img width="12" height="12" src="img/icons/map/tourist_zoo.p.12.png" /> zoos</a></li>
        
          <li><img width="12" height="12" src="img/icons/map/transport_bus_stop.p.12.png" /> Transportation</a></li>
          <li><a onClick='store("aeroway=aerodrome")'><img width="12" height="12" src="img/icons/map/transport_airport2.p.12.png" /> airports</a></li>
          <li><a onClick='store("highway=bus_stop")'><img width="12" height="12" src="img/icons/map/transport_bus_stop.p.12.png" /> bus stops</a></li>
          <li><a onClick='store("tourism=camp_site")'><img width="12" height="12" src="img/icons/map/accommodation_camping.p.12.png" /> camp site</a></li>
          <li><a onClick='store("amenity=fuel")'><img width="12" height="12" src="img/icons/map/transport_fuel.p.12.png" /> fuel</a></li>
          <li><a onClick='store("amenity=parking")'><img width="12" height="12" src="img/icons/map/transport_parking.p.12.png" /> parking</a></li>
          <li><a onClick='store("highway=traffic_signals")'><img width="12" height="12" src="img/icons/map/transport_traffic_lights.p.12.png" /> traffic signals</a></li>
          <li><a onClick='store("railway=station")'><img width="12" height="12" src="img/icons/map/transport_train_station2.p.12.png" /> train stations</a></li>
        
          <li><i class="icon-eye-open"></i> Big Brother</li>
          <li><a onClick='store("man_made=surveillance")'><i class="icon-eye-open"></i> surveillance cameras</a></li>
          <li><a onClick='store("highway=speed_camera")'><i class="icon-camera"></i> speed cameras</a></li>
      </ul>
    </div>
    in the selected region and
    <button class="btn btn-default btn-sm" onClick='displayResult();'><i class="icon-refresh"></i> update data</button>.
    <p>
    <pre class="prettyprint" style="overflow:auto; height:290px;" id="transformResult">&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;kml xmlns="http://earth.google.com/kml/2.2"&gt;
&lt;Document&gt;
  &lt;Placemark&gt;
  &lt;name&gt;Gelaterie Genovesi&lt;/name&gt;
  &lt;Point&gt;
   &lt;coordinates&gt;8.9536548,44.4078128,0&lt;/coordinates&gt;
  &lt;/Point&gt;
 &lt;/Placemark&gt;
&lt;/Document&gt;
&lt;/kml&gt;</pre>
    <button class="btn btn-default" onClick='selectAll("transformResult");'>Select All</button>
    <a id="kml-download" href="#"><button class="btn btn-default"><i class="icon-download"></i> Download .kml</button></a>
    <button class="btn btn-info pull-right" data-toggle="collapse" data-target="#techno" style="margin-left:50px;">Technical Notes</button>
    </p>
  </div>
    <div class="col-md-6">
  <div id="map" style="height:400px;">
  <script type="text/javascript">
    map = new L.Map('map');
    const osmUrl='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    const osmAttrib='Map data © OpenStreetMap contributors';
    const osm = new L.TileLayer(osmUrl, {minZoom: 4, maxZoom: 16, attribution: osmAttrib});

    map.setView(new L.LatLng(44.410499,8.95188),16);
    map.addLayer(osm);
    map.on('moveend', updateURL);

    prepareDownload(document.getElementById('transformResult').innerHTML.replace(/&lt;/g, '<').replace(/&gt;/g, '>').replace(/&/g, '&amp;'));

    L.control.locate().addTo(map);
    prettyPrint();
  </script>
  </div>
  </div>
  <input type="hidden" id="myurl" value="https://overpass-api.de/api/xapi?node[bbox=8.9473557472229,44.40677612881456,8.957226276397705,44.41290746538075][amenity=ice_cream]" />
  <input type="hidden" id="rawkml" />
  </div>

  <div id="techno" class="collapse out">
  <h4>Technical Notes</h4>
  <p>OpenStreetMap data is retrieved via <a href="https://wiki.openstreetmap.org/wiki/Overpass_API">Overpass API</a> from <tt>overpass-api.de</tt>. The server can handle both small and large requests. The latter will take longer, so expect some delay when querying data for large regions.</p>
  <p class="text-warning">The conversion from OpenStreetMap XML to KML is performed by your browser. It needs to support XSL transformations (modern browsers do). Queries for large regions can slow your browser down or crash it. Syntax highlighting is only enabled for small KML files for similar reason.</p>
  <p class="text-warning">Some points of interest have several alternative taggings in OpenStreetMap. The KML generator may miss some of them. Do not assume the results to be exhaustive; the KML generator is more a playground tool than meant for serious projects.</p>
  </div>

  </div> <!-- /container -->

  <?php include "footer.inc"; ?>

  <script>
  $(function () { $("a[rel=popover]").popover();});
  $(document).on('click','.dropdown-menu a',function(){
    document.getElementById('current-poi-type').innerHTML = $(this).html();
  });
  </script>
  </body>
</html>
