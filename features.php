<!DOCTYPE html>
<html lang="en">
<?php include "head.inc"; ?>
<body>
<?php include "navigation.inc"; ?>

  <div class="container">
    <div class="col-md-12">
      <p>Explore the neighborhood with Marble's rich set of city and street level maps. Search for addresses and places of interest. Marble takes care of querying various search backends and presents their results in a unified view. Calculate pedestrian, bike and motorcar routes with ease &mdash; online and offline, with an arbitrary number of via points.</p>
    </div>
  <!-- TODO: Second set of screenshots for search, osm themes, routing here -->
    <div class="col-md-4"><img class="img-thumbnail" src="img/gallery/osm-themes.png" alt="" /></div>
    <div class="col-md-4"><img class="img-thumbnail" src="img/gallery/search-result-list.png" alt="" /></div>
    <div class="col-md-4"><img class="img-thumbnail" src="img/gallery/routing-osm.png" alt="" /></div>

    <div class="col-md-12">
      <p>Start exploring the world. View clouds and sun shadow, follow satellites and space stations and display their orbits, all updated in real-time. Travel back in time and learn about historic views of our planet using maps from past centuries. Earth is not enough? Marble also offers maps of the moon and other planets.</p>
    </div>
    
    <div class="col-md-5"><img class="img-thumbnail" src="img/gallery/historic-map-themes.jpg" alt="" /></div>
    <div class="col-md-4"><img class="img-thumbnail" src="img/gallery/satellites-animation-iss.gif" alt="" /></div>
    <div class="col-md-3"><img class="img-thumbnail" src="img/gallery/planets.jpg" alt="" /></div>
    <div class="col-md-3"><img class="img-thumbnail" src="img/gallery/topo-region.jpg" alt="" /></div>
    <div class="col-md-4"><img class="img-thumbnail" src="img/gallery/climate-zones.png" alt="" /></div>

    <div class="col-md-12">
  
  <!--
  <p>A detailed description of features with screenshots is available in the <a href="changelog.php">Visual Changelogs</a> that accompany each Marble release.</p>
  -->

  <h3>Feature Matrix</h3>
  <p>Built on a common foundation, Marble shares a similar feature set on all supported platforms. Compare their similarities and differences in the feature matrix below.</p>

<table class="table table-striped table-bordered table-hover table-condensed">

<?php

function print_cell($style, $desc, $hint)
{
  print "<td class=\"" .$style . "\">";
  if ($hint != "") {
    print "<a style=\"cursor:pointer\" rel=\"popover\" data-placement=\"top\" data-content=\"" . $hint . "\">";
  }
  if ($desc == "yes") {
    print "<i class=\"icon-ok\"></i></td>\n";
  } elseif ($desc == "no") {
    print "<i class=\"icon-minus-sign\"></i>\n";
  } else {
    print $desc;
  }
  if ($hint != "") {
    print "</a>\n";
  }
}

function print_row($title, $description, $kde_style, $kde_desc, $kde_hint, $n900_style, $n900_desc, $n900_hint)
{
  print "<tr>\n";
  print "<th><a style=\"cursor:pointer\" rel=\"popover\" data-placement=\"right\" title=\"" . $title . "\" ";
  print "data-content=\"" . $description . "\">" . $title . "</a></th>\n";
  print_cell($kde_style, $kde_desc, $kde_hint);
  print_cell($n900_style, $n900_desc, $n900_hint);
  print "</tr>\n";
}

function print_head_row($title)
{
  //print "<tr>\n<th colspan=\"6\">" . $title . "</th>\n</tr>\n";
  print "<tr>\n<th style=\"padding-top:15px\">" . $title . "</th>\n";
  print "<th style=\"padding-top:15px\">Desktop (KDE/Qt)</th>\n";
  print "<th style=\"padding-top:15px\">Android (Beta)</th>\n</tr>\n";
}

  print_head_row("Map Presentation");
  print_row("Globe View", "The map is shown on a sphere &mdash; like the globe on your desktop",
     "yes", "yes", "",
     "no", "no", "");
  print_row("Atlas View", "The map is shown flat &mdash; like an atlas",
     "yes", "yes", "",
     "no", "no", "");
  print_row("Street Maps", "Maps with detailed information on city/street level",
     "yes", "yes", "",
     "yes", "yes", "");
  print_row("Satellite Maps", "Maps composed from aerial and satellite images",
     "yes", "yes", "",
     "no", "no", "");
  print_row("Topographic Maps", "Maps indicating the terrain (relief)",
     "yes", "yes", "",
     "no", "no", "");
  print_row("Educational Maps", "Temperature and precipitation maps",
     "yes", "yes", "",
     "no", "no", "");
  print_row("Other Planets", "Maps for planets (and moons) other than earth",
     "yes", "yes", "",
     "no", "no", "");
  print_row("Further Maps", "Additional map themes: OpenStreetMap derived maps, further planets, more historic maps, ...",
     "opt", "in-app download", "Available via GHNS (File => Download Maps)",
     "no", "no", "");

  print_head_row("Information Layers");
  print_row("Weather", "Show regional weather information",
     "yes", "yes", "",
     "no", "no", "");
  print_row("Real-time clouds", "Show current cloud coverage",
     "yes", "yes", "",
     "no", "no", "");
  print_row("Day/Night View", "Sun shadowing based on day time",
     "yes", "yes", "",
     "no", "To be added", "");
  print_row("Real-time satellites", "Show real-time satellite position and orbits",
     "yes", "yes", "",
     "no", "no", "");
  print_row("Wikipedia Articles", "Show wikipedia articles",
     "yes", "yes", "",
     "no", "no", "");
  print_row("Photos", "Show photos",
     "yes", "yes", "",
     "no", "no", "");
  print_row("Postal Codes", "Show postal codes",
     "yes", "yes", "",
     "no", "no", "");
  print_row("Earthquakes", "Show recent/historic earthquakes",
     "yes", "yes", "",
     "no", "no", "");
  print_row("Real-time traffic", "Display of traffic jams and related information",
     "no", "no", "",
     "no", "no", "");

  print_head_row("Search");
  print_row("Online address search", "Address search using online services like OpenStreetMap Nominatim",
     "yes", "yes", "",
     "yes", "yes", "");
  print_row("Online POI search", "Point of interest search using online services like OpenStreetMap Nominatim",
     "yes", "yes", "",
     "yes", "yes", "");
  print_row("Offline address search", "Offline address search.",
     "opt", "in-app download", "Regional data must be installed beforehand by installing regional routing data from within Marble",
     "no", "To be added", "");
  print_row("Offline POI search", "Offline search for points of interest. Regional data must be installed beforehand by installing regional routing data from within Marble.",
     "opt", "in-app download", "Regional data must be installed beforehand by installing regional routing data from within Marble",
     "no", "To be added", "");

  print_head_row("Positioning & Tracking");
  print_row("GPS localization", "Position determination using GPS",
     "yes", "yes", "",
     "yes", "yes", "");
  print_row("WLAN localization", "Position determination using wireless networks",
     "opt", "Open WLAN Map", "Requires installation of libwlocate",
     "yes", "yes", "");
  print_row("Track export (kml)", "Save the track as a .kml file",
     "yes", "yes", "",
     "no", "To be added", "");
  print_row("Track export (gpx)", "Save the track as a .gpx file",
     "no", "manual", "Manual conversion of a .kml track using gpsbabel",
     "no", "To be added", "");

  print_head_row("Routing & Navigation");
  print_row("Online motorcar routing", "Calculate routes using several online services as backend",
     "yes", "yes", "",
     "yes", "yes", "");
  print_row("Online bike routing", "Calculate routes using several online services as backend",
     "yes", "yes", "",
     "yes", "yes", "");
  print_row("Online pedestrian routing", "Calculate routes using several online services as backend",
     "yes", "yes", "",
     "yes", "yes", "");
  print_row("Online transit routing", "Calculate routes using several online services as backend",
     "no", "no", "",
     "no", "no", "");
  print_row("Offline motorcar routing", "Calculate routes offline on the device",
     "opt", "in-app download", "Requires installation of monav-routing-daemon. Regional data must be installed beforehand by installing regional routing data from within Marble.",
     "no", "To be added", "");
  print_row("Offline bike routing", "Calculate routes offline on the device",
     "opt", "in-app download", "Requires installation of monav-routing-daemon. Regional data must be installed beforehand by installing regional routing data from within Marble.",
     "no", "To be added", "");
  print_row("Offline pedestrian routing", "Calculate routes offline on the device",
     "opt", "in-app download", "Requires installation of monav-routing-daemon. Regional data must be installed beforehand by installing regional routing data from within Marble.",
     "no", "To be added", "");
  print_row("Offline transit routing", "Calculate routes offline on the device",
     "no", "no", "",
     "no", "no", "");
  print_row("Eco routing", "Calculation of most fuel-economic routes",
     "no", "no", "",
     "no", "no", "");
  print_row("Real-time traffic", "Usage of real-time traffic information in route calculation",
     "no", "no", "",
     "no", "no", "");
  print_row("Arbitrary via points", "Create as many via points as you like",
     "yes", "yes", "",
     "yes", "yes", "");
  print_row("Elevation profile", "Show the elevation profile of the route",
     "yes", "yes", "",
     "no", "no", "");
  print_row("Alternative routes", "Choose among several alternative routes",
     "yes", "yes", "",
     "no", "To be added", "");
  print_row("Turn-by-turn navigation", "Upcoming turns are announced",
     "yes", "yes", "",
     "yes", "yes", "");
  print_row("Voice navigation", "Spoken turn announcements",
     "yes", "yes", "",
     "yes", "yes", "");
  print_row("Regional speakers", "Voice navigation in your mother tongue",
     "opt", "in-app download", "Automatic installation upon selection",
     "no", "To be added", "");

  print_head_row("Tools");
  print_row("Distance measurement", "Measure distances between two or more points",
     "yes", "yes", "",
     "no", "no", "");
  print_row("Bookmarks", "Create bookmarks for your favorite places",
     "yes", "yes", "",
     "no", "To be added", "");
  print_row("Time Simulation", "Travel back/forth in time and change the speed of time",
     "yes", "yes", "",
     "no", "no", "");
  print_row("Offline Mode", "Prevent any non-interactive Internet access",
     "yes", "yes", "",
     "no", "To be added", "");


?>

  </table>

  </div>
  </div>
  <!-- /container -->

  <?php include "footer.inc"; ?>

  <script>
$(function ()
{ $("a[rel=popover]").popover();});
</script>
  </body>
</html>
