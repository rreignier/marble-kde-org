<!DOCTYPE html>
<html lang="en">
<?php include "head.inc"; ?>
<body>
<?php include "navigation.inc"; ?>
  <div class="container">
  
  <h3>opencaching.com login</h3>
  <p class="lead">You are now logged in to your opencaching.com account. Close this page to continue.</p>

  </div>
  <!-- /container -->

  <?php include "footer.inc"; ?>

  </body>
</html>
