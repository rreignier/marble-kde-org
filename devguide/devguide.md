% Marble Developers Guide
% Dennis Nienhüser
% August 16th, 2014

Welcome! This guide assists in setting up Marble in development environments
and provides a detailed overview of the Marble library.
It is directed mostly at

* developers who want to use the Marble library in their project
* packagers who want to generate binary packages for installing Marble and
* contributors who want to help making Marble yet more versatile and polished.

End users might find some useful information also,
but are often better served just [installing](http://marble.kde.org/install.php) Marble
and, if needed, reading the [end-user documentation](http://marble.kde.org/documentation.php) or
requesting [support](http://marble.kde.org/support.php).

The Marble project consists of software (LGPL 2.1 license) and data (some free license).
The vast majority of its features are contained in the library portions (`libmarblewidget`, `libastro` and `plugins` sum up to 140k SLOC)
and hence available to 3rd party applications.
The Marble application end-users typically interact with (Marble KDE or Marble Qt) are each just thin wrappers that expose the library features, each less than 3k SLOC.
A high-level overview of the existing modules looks like follows:

![Marble Modules](img/dev/modules.png)

The remaining parts of the developers guide are organized into three chapters.
[Building Marble](#building) describes how to retrieve the Marble sources, build them,
set up a development environment, install Marble and create packages/installers.
The [Architecture and Concepts](#architecture) looks into the code contained within
the library portions: What are the most important classes, how do they interact?
Finally we consider how to extend Marble by new plugins, map themes and how to embed it
into 3rd party applications in the [Extending Marble](#extending) chapter.
