# Architecture and Concepts {#architecture}

## MarbleWidget {#marblewidget}
`MarbleWidget` is a `QWidget` that shows a map and associated information.
For many use cases it is the main entry point into the `marblewidget` library.
Many properties and methods are available to customize the appearance of the widget for your needs.
By default `MarbleWidget` is interactive, allowing users to change which parts of the planet
are displayed.

### Minimalistic Code Example
    #include <QApplication>
    #include <marble/MarbleWidget.h>

    int main(int argc, char** argv)
    {
        QApplication app(argc, argv);
        Marble::MarbleWidget *mapWidget = new Marble::MarbleWidget;
        mapWidget->setMapThemeId("earth/srtm/srtm.dgml");
        mapWidget->show();
        return app.exec();
    }
Thanks to using sane default values for all properties a basic `MarbleWidget` is easy to setup.
The only thing that needs to be set is the `mapThemeId` property as explained in the [Map Themes](#mapthemes) section.
The code above, once compiled and executed, results in an application that looks like this:

![MarbleWidget Screenshot](img/dev/devguide-marblewidget-1.png)

### Most Common Properties and Methods

While `MarbleWidget` can easily be created, it also provides a versatile configuration. Among the commonly changed properties and methods are:

* Choosing a planet to display and how it is rendered with the help of the `mapThemeId` property. See the [Planets and Map Themes](#mapthemes) section for details.
* Deciding whether the map is shown as a globe or as some variant of a flat map through the `projection` property. Most commonly `Marble::Spherical` is used to render a globe and `Marble::Mercator` for a "flat" map.
* Adjusting the part of the map that is shown by the `zoom`, `longitude` and `latitude` parameters or one of their convenience methods. See the [Viewport and Projections](#viewport) section for details.
* Changing which elements are shown and how they are displayed by the various variants of `show*` properties as well as the `floatItems` and `renderPlugins` methods.
* Fine-tuning how the user can interact with `MarbleWidget` using the `popupMenu` and `inputHandler` methods, or disabling input completely using `setInputEnabled(false)`.
* Adding your own rendering in a custom map layer using the `addLayer` method. The [Map Layers](#layers) section covers this in detail.
* Managing data related properties by accessing properties and methods of `MarbleModel` via the `model` property. See [MarbleModel](#marblemodel) for details.

### Qt Designer Integration

'MarbleWidget' can be used from within Qt Designer as described in [this section](#qtdesigner).

## MarbleModel {#marblemodel}

## Planets and Map Themes {#mapthemes}
While most importantly used to display maps of planet Earth, Marble can render maps of other planets or moons just as fine.
All it takes is a [DGML](#dgml) file which assembles the associated image and/or vector data and provides meta data like a name or the radius of the planet.
Such a DGML file can be loaded using `MarbleWidget::setMapThemeId`; the associated map theme is loaded as well as the corresponding planet.

Typically you do not have to deal with planets directly, however.
Instead one of the ready-made map themes can easily be loaded for display in [MarbleWidget](#marblewidget) using its `mapThemeId` property.
Map theme IDs follow a `$planet/$theme/$theme.dgml` naming scheme.
Check out the `data/maps/` directory in the Marble sources for a list of default map themes.
A typical Marble installation provides the following ones:

Preview                                                 Name                            ID                                      Description
------------------------------------------------        ------------------------        --------------------------------------  --------------------------------------------------
![OSM](img/dev/themes/openstreetmap-preview.png)        OpenStreetMap                   earth/openstreetmap/openstreetmap.dgml  The free Wiki world map
![OSM](img/dev/themes/srtm-preview.png)                 Atlas                           earth/srtm/srtm.dgml                    A vector map in Atlas style
![OSM](img/dev/themes/bluemarble-preview.png)           Satellite View                  earth/bluemarble/bluemarble.dgml        Earth seen from space
![OSM](img/dev/themes/political-preview.png)            Political Map                   earth/political/political.dgml          Highlights governmental boundaries with colors
![OSM](img/dev/themes/citylights-preview.png)           Earth at Night                  earth/citylights/citylights.dgml        Earth seen from space at night
![OSM](img/dev/themes/plain-preview.png)                Plain Map                       earth/plain/plain.dgml                  A minimalistic vector map
![OSM](img/dev/themes/schagen1689-preview.png)          Historical Map 1689             earth/schagen1689/schagen1689.dgml      More than 400 years old
![OSM](img/dev/themes/temp-july-preview.png)            Temperature (July)              earth/temp-july/temp-july.dgml          Average temperature in July
![OSM](img/dev/themes/temp-dec-preview.png)             Temperature (December)          earth/temp-dec/temp-dec.dgml            Average temperature in December
![OSM](img/dev/themes/precip-july-preview.png)          Precipitation (July)            earth/precip-july/precip-july.dgml      Average precipitation in July
![OSM](img/dev/themes/precip-dec-preview.png)           Precipitation (December)        earth/precip-dec/precip-dec.dgml        Average precipitation in December
![OSM](img/dev/themes/clementine-preview.png)           Moon                            moon/clementine/clementine.dgml         Earth's moon recorded by the Clementine spacecraft
------------------------------------------------        ------------------------        --------------------------------------  --------------------------------------------------

Further map themes are available at [marble.kde.org](http://marble.kde.org/maps-v3.php).
The Marble applications provide built-in support to easily explore and install them.
You can add support for it as well in your application using the `MapThemeDownloadDialog` class.
Other popular maps like Google Maps, Google Satellite, Bing Maps and more can easily be integrated into Marble as well from a technical point of view.
From a legal point of view however this is likely problematic and we do not want to promote the usage of maps which we think are not free.

Programmatic access to the list of installed map themes is provided through the `MapThemeManager` class,
which provides a `QAbstractItemModel` for convenience.

Marble does know about a dozen different planets by itself.
They can be accessed using `PlanetFactory`.
For example, `PlanetFactory::construct("earth")` creates an instance of planet Earth.
The `Planet` class is a lightweight class that contains a handful properties mostly needed for calculations.
For planets yet unknown to Marble you can instantiate a `Planet` directly and set the respective properties.
Alternatively new planets can also be instantiated from DGML files.
This enables dynamic extension of the list of planets without the need to recompile Marble.

## Rendering in Map Layers {#layers}

## Viewport and Projections {#viewport}

The `zoom` property has no unit, but its related properties `distance` and `radius` have:
`distance` is the distance in `km` between the map surface and the virtual camera (increasing the distance zooms out), `radius` is the radius of the full globe in pixel (increasing the radius zooms in).
These three properties are related and changing any of them changes the other two automatically.
The `longitude` and `latitude` properties are accompanied by several convenience `centerOn` methods which take points, places or rectangular regions as arguments.

## The Tree Model

## Geographic Scene

## Runners

## Search

## Routing

## Location

## Plugins

## Float Items

## DGML

## KML

## GeoData

## Python Bindings

