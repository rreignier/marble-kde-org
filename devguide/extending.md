# Extending Marble {#extending}

## Writing Map Themes

## Writing Plugins

## Integrating Marble into Applications

## Using Qt Designer Plugins {#qtdesigner}

When Marble is build with the `WITH_DESIGNER_PLUGIN=TRUE` flag (activated by default),
`MarbleWidget` is available from within Qt Designer and can be embedded into `.ui` files just like any other `QWidget`.
If `MarbleWidget` does not show up in Qt Designer, check that:

* The boolean cmake option WITH_DESIGNER_PLUGIN is activated
* The designer plugin is installed in a directory that is among Qt Designer's plugin paths
* The running Qt Designer has access to the `marblewidget` library through `LD_LIBRARY_PATH` or a comparable library search path setup.

<p class="bg-warning">Please note that `MarbleWidget` will not show up in Qt Creator's Design Pane. You have to use Qt Designer instead.</p>

## Contributing Back

### Generating Patches

### Reviewboard
