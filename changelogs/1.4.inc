<div>
  <h3>Marble 1.4</h3>
  <p>The first release of Marble in the 1.4 series, Marble 1.4.0, was released on August 1st, 2012 alongside the KDE software compilation. Further 1.4.x versions follow the KDE 4.9.x releases. In the good tradition of recent releases, we have collected those changes directly visible to the user. Please enjoy looking over the new and noteworthy.</p>

  <h3>User Interface Enhancements</h3>
  <p>Some gradual improvements to the user interface make Marble more pleasant to use on the Desktop. The search bar has been moved from the Navigation tab to the main toolbar for easier access. Suggestions come up while you type.</p>
  <p>Fonts used in maps now use an outline effect for better readability. Your bookmarks are now also shown in the map (unless you deactivate that in the <tt>Bookmarks</tt> menu).</p>
  <p>Compare the two screenshots below &mdash; showing Marble 1.3 on top and Marble 1.4 below &mdash; to see the differences.</p>
  <img class="thumbnail" src="img/screenshots/1.4/marble-ui-13-vs-14.png" />


  <h3>FlightGear Support</h3>
  <p>Marble can now show the position of an airplane simulated by FlightGear.</p>
  <iframe width="560" height="315" src="http://www.youtube.com/embed/-yQcJacLtLc" frameborder="0" allowfullscreen="true"></iframe>

  <h3>Routing Extensions</h3>
  <p>A number of gradual improvements in the routing area lead to an overall enhanced user experience. Just like it was possible in the mobile versions before, Marble's desktop version now let's you open and save routes as KML files. New turn instruction types and voice commands are supported and tasks like reversing or clearing the route got their own buttons to reduce the number of clicks needed to accomplish common tasks. Support for MapQuest and the Open Source Routing Machine (OSRM) has been added. Just like the existing five routing backends they are queried (if enabled) in the background to deliver alternative routes you can choose from. Both OSRM and MapQuest support worldwide routing; OSRM is known for speedy results thanks to its underlying fast routing algorithm (contraction hierarchies), while MapQuest carefully analyzes your route to generate versatile turn directions.
  </p>
  <a href="img/screenshots/1.4/marble-routing.png"><img src="img/screenshots/1.4/marble-routing_thumb.png" /></a>

  <h3>Postal Codes Online Service</h3>
  <p>With the paperless office still to arrive snail mail plays an ever important role in daily life. Part of the addressing scheme in nearly all countries worldwide are postal codes (also known as post codes, ZIP codes or PIN codes). Each postal code usually covers a certain geographical area. Marble can now help you visualizing these areas and identifying their spatial relationship once you activate the postal code plugin. It was developed by Valery Kharitonov during Google Code-In 2011 to join Marble's list of online services.</p>
  <img src="img/screenshots/1.4/marble-postal-codes.png" />
  <p>Use the postal code plugin to show local postal codes on top of any map theme.</p>

  <h3>Basic ESRI Shapefile Support</h3>
  <p>Now Marble sports initial support for loading ESRI Shapefile polygons. This is done via a dedicated file loading plugin. In order to make use of this feature the Marble compile needs to link against libshp (http://shapelib.maptools.org). Note that at the current point there is no styling done.</p>

  <h3>Further Changes</h3>
  <p>Another nifty addition to the latest version of Marble is support for the logfiles of the TangoGPS application.</p>

  <h3>Factsheet</h3>

  There's a factsheet available that gives an overview for the latest full feature set.
  <div class="btn-group">
    <button class="btn btn-default btn-sm">English version</button>
    <button class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
      <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
        <li><a href="http://www.slideshare.net/marbleglobe/marble-virtual-globe-14-factsheet-english">English version</a></li>
        <li><a href="http://www.slideshare.net/marbleglobe/marble-virtual-globe-14-factsheet-spanish">Spanish version</a></li>
        <li><a href="http://www.slideshare.net/marbleglobe/marble-virtual-globe-14-factsheet-german">German version</a></li>
        <li><a href="http://www.slideshare.net/marbleglobe/marble-1-4fr">French version</a></li>
    </ul>
  </div>

<!--          <div class="btn-group">
    <a href="http://www.slideshare.net/marbleglobe/marble-for-developers-factsheet"><button class="btn btn-default">For Developers</button></a>
  </div>-->

  <p></p>

  <iframe src="http://www.slideshare.net/slideshow/embed_code/14554799" width="800" height="400" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe>

</div>
