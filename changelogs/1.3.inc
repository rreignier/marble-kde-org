<div>
    <h3>Marble 1.3</h3>
    <p>Marble 1.3.0 was released on January 25th, 2012 as part of the KDE 4.8 release. See the Download section for Marble packages. In the good tradition of recent releases, we have collected those changes directly visible to the user. Please enjoy looking over the new and noteworthy:</p>

    <h3>Elevation Profiles</h3>
    <p>Estimating the incline is important to hikers and bikers when planning a route. Marble's new elevation profile shows the height profile of any route worldwide in an interactive info box. After activating the Elevation Profile info box in Marble's menu, height profiles are automatically displayed and updated as you change the route. This feature was developed by Niko Sams and Florian Eßer. It is available in all Marble versions (Desktop and Mobile).</p>
    <iframe width="560" height="315" src="http://www.youtube.com/embed/sIZB35MWZrI" frameborder="0" allowfullscreen="true"></iframe>
    <p>Do you like steep routes? Or rather avoid them? Marble's new elevation profile will help you in either case.</p>

    <h3>Satellites - Earth Seen from Space</h3>
    <p>Did you ever wonder how position tracking works in Marble? The answer, as you may already know, is GPS, the Global Positioning System. One of its core parts are satellites traveling around the earth. You might be interested where those are, since their position highly influences the accuracy of your estimated current position. Guillaume Martres helps answering that question: Activate the Satellites Online Service and Marble shows you many of the artificial satellites. You can configure which satellite types are visible (like GPS or weather related ones), show their orbit and even select any of them to attend in a virtual flight!</p>
    <p>This feature was developed during an ESA SOCIS project, the first Summer of Code in Space offered this year by the European Space Agency (ESA). We'd like to thank both Guillaume for his excellent work and the ESA for offering us the chance to be one of the mentoring organizations. Satellites can be viewed in Marble's Desktop versions and in Marble Touch on the Nokia N9/N950.</p>
    <iframe width="560" height="315" src="http://www.youtube.com/embed/YlKMPEAf-mk" frameborder="0" allowfullscreen="true"></iframe>
    <p>Marble can now display satellites from many different categories, show their orbits and even track them.</p>

    <h3>Plasma Runner Integration</h3>
    <p>KRunner, the "Alt+F2" tool of Plasma, helps searching and launching files and applications. Thanks to Friedrich Kossebau it is now aware of Marble and can open its bookmarks and coordinates. So next time you stumble upon some coordinates on a website or get an invitation to a party by email, just paste the coordinates in krunner and view the place in Marble.</p>
    <iframe width="560" height="315" src="http://www.youtube.com/embed/auxbMI4N6is" frameborder="0" allowfullscreen="true"></iframe>
    <p>Open your Marble bookmarks and any coordinates from krunner.</p>

    <h3>Extended OpenStreetMap Vector Rendering</h3>
    <p>Similar to how the Wikipedia community gathered an incredible amount of knowledge outperforming commercial encyclopedias, the OpenStreetMap contributors are busy mapping each and every part of the planet to create the data for the many OpenStreetMap based maps. Marble integrated them early (e.g. Mapnik, Open Cycle Map, Hike &amp; Bike Map, Osmarender) in the popular server based tiling schemes where the map consists of many small images placed next to each other similar to a puzzle. This approach has several advantages, but customizations of the final map are hardly doable. Moreover the images for larger regions require a huge amount of disk space which makes offline usage difficult. While this is often not an issue for desktop computers, things change for mobile devices. And since we plan to bring Marble to the devices you may be using today or tomorrow, we are working on OpenStreetMap vector rendering support that needs little disk space, little computational effort and can be customized to the context you use it in (like motorcar navigation or hiking).</p>
    <p>Towards this goal Konstantin Oblaukhov made great progress during his Google Summer of Code project. Marble can now open .osm files and show a huge amount of the elements they contain.</p>
    <iframe width="560" height="315" src="http://www.youtube.com/embed/QnZDkxj2SUU" frameborder="0" allowfullscreen="true"></iframe>
    <p>Many OpenStreetMap elements can now be imported and rendered by Marble on street level.</p>
    <p>Of course we plan to continue working on the OpenStreetMap vector rendering support such that future Marble versions improve the rendering on different zoom levels, can be customized and download the data automatically from KDE servers. Stay tuned.</p>

    <h3>Marble Touch - Marble on the Nokia N9/N950</h3>
    <p>Marble's first end-user ready mobile version made its debut on the Nokia N900 in 2010. Aiming to create a more fluid and fun to use predecessor, we have been working on Marble Touch, a mobile version of Marble that is based on new Qt technologies (Qt Quick and Qt Components) and runs on the Nokia N9/N950. A prominent change is the introduction of activities which optimize the user interface towards common tasks like searching or routing.</p>
    <img src="img/screenshots/1.3/marble-touch-routing.jpg" />
    <p>Marble Touch on the Nokia N950 in the Routing activity.</p>
    <p>Please note that the initial version 1.3.0 of Marble Touch does not include all features the N900 version of Marble offers. We do plan however to add these and other features gradually in monthly feature releases.</p>
    <p><strong>Update:</strong>Marble Touch is now available in the <a href="http://store.ovi.com/content/249807">Nokia Store</a>.</p>
    <iframe width="560" height="315" src="http://www.youtube.com/embed/iCrcWyYaiwo" frameborder="0" allowfullscreen="true"></iframe>
    <p>Marble Touch runs on the Nokia N9/N950. Versions for Plasma Active and Android are planned for the future.</p>
    <p>Even though Marble Touch is deemed to be the predecessor of the Maemo version, we still maintain the wide-spread N900 version, which is released in version 1.3 in parallel. Future plans for Marble Touch include optimizations for tablet devices. Thanks to the usage of Qt Components and thanks to the Plasma Active team working on compatible Plasma Active Components, Marble Touch on Plasma Active will follow soon. A version for Android is planned as well.</p>

    <h3>And More ...</h3>
    <ul>
    <li>New Maps for Download are available: OpenStreetMap themes, historic maps and maps for other planets, most of them created by students during Google Code-In. You can install them immediately using the Get New Maps feature available from Marble's File menu or the Preferences page in Marble Touch.</li>
    <li>The number of voice navigation speakers has risen considerably, so chances are high you're able to listen to Marble giving you navigation instructions in your native language. Check out the Voice Guidance Speakers website to see (and hear) them all.</li>
    <li>Your virtual globe now behaves even more like a desktop globe thanks to kinetic spinning implemented by Ariya Hidayat. Watch e.g. the Satellites screencast above to see it in action (compare the behavior of the mouse to the movement of the globe).</li>
    <li>Two new GPS related info boxes have been added. Just like other info boxes, they can be enabled in the View / Info Boxes menu.</li>
    <li>Support for file formats has been modularized; parsing is now done in threaded runners. Developers can perform routing tasks from the library.</li>
    <li>As usual, many bugfixes, performance improvements, enhancements and refactorings under the hood have been done in order to keep our code in best shape ;-) 26 developers made 1031 commits, adding 66,291 and removing 55,948 lines of code. Marble in KDE 4.8 now measures 99,489 source lines of code in total, 13,544 more than in KDE 4.7 (generated using David A. Wheeler's 'SLOCCount').</li>
    </ul>

    <h3>Factsheet</h3>

    <p>There's a factsheet available that gives an overview for the latest full feature set.</p>
    <div class="btn-group">
      <button class="btn">For Users: English version</button>
      <button class="btn dropdown-toggle" data-toggle="dropdown">
        <span class="caret"></span>
      </button>
      <ul class="dropdown-menu">
          <li><a href="http://www.slideshare.net/marbleglobe/marble-virtual-globe-13-factsheet-english">English version</a></li>
          <li><a href="http://www.slideshare.net/marbleglobe/marble-virtual-globe-13-factsheet-spanish">Spanish version</a></li>
          <li><a href="http://www.slideshare.net/marbleglobe/marble-virtual-globe-13-factsheet-german">German version</a></li>
          <li><a href="http://www.slideshare.net/marbleglobe/marble-virtual-globe-13-factsheet-french">French version</a></li>
          <li><a href="http://www.slideshare.net/marbleglobe/marble-virtual-globe-marble-virtual-globe-13-factsheet-hindi">Hindi version</a></li>
      </ul>
    </div>

    <div class="btn-group">
      <a href="http://www.slideshare.net/marbleglobe/marble-for-developers-factsheet"><button class="btn btn-default">For Developers</button></a>
    </div>

    <p></p>

    <iframe src="http://www.slideshare.net/slideshow/embed_code/10742037" width="800" height="400" frameborder="0" marginwidth="0" marginheight="0" scrolling="no"></iframe>
</div>
