<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml">

<xsl:output method="xml" indent="yes" encoding="UTF-8"/>

<xsl:template match="/osm">&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;kml xmlns="http://earth.google.com/kml/2.2"&gt;
&lt;Document&gt;
<xsl:apply-templates select="node">
            <xsl:sort select="name" />
        </xsl:apply-templates>&lt;/Document&gt;
&lt;/kml&gt;
</xsl:template><xsl:template match="node">  &lt;Placemark&gt;<xsl:for-each select="tag[@k='name']">
  &lt;name&gt;<xsl:value-of select="@v"></xsl:value-of>&lt;/name&gt;</xsl:for-each>
  &lt;Point&gt;
   &lt;coordinates&gt;<xsl:value-of select="@lon"></xsl:value-of>,<xsl:value-of select="@lat"></xsl:value-of>,0&lt;/coordinates&gt;
  &lt;/Point&gt;
 &lt;/Placemark&gt;
</xsl:template>

</xsl:stylesheet>
