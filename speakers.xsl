<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
  version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns="http://www.w3.org/1999/xhtml">
 
<xsl:output method="xml" indent="yes" encoding="UTF-8"/>
 
<xsl:template match="/knewstuff">
    <table class="table table-hover">
    <tr>
        <th>Gender</th>
        <th>Description</th>
        <th>Preview and Download</th>
    </tr>
        <xsl:apply-templates select="stuff">
            <xsl:sort select="name" />
        </xsl:apply-templates>
    </table>
</xsl:template>
 
<xsl:template match="stuff">
    <tr>
        <td><img src="{preview}" style="vertical-align:middle;" /></td>
        <td style="vertical-align:middle;"><p>
        <b><xsl:value-of select="name"/></b><br />
        <i><xsl:value-of select="summary"/></i><br />
        License: <xsl:value-of select="licence"/></p></td>
        <td><audio src="{payload[@lang='ogg']}" controls="controls">Your browser does not support the audio element.</audio><br />
        <a href="{payload[@lang='zip']}" ><i class="icon-download"><span/></i> Marble</a> <span style="margin-left:20px;" /><a href="{payload[@lang='tomtom']}" ><i class="icon-download"><span/></i> TomTom</a></td>
    </tr>
</xsl:template>
 
</xsl:stylesheet>
